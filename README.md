# Purposes
This repository provides a frontend to the [RAGChat-API](https://gitlab.rrz.uni-hamburg.de/zbhai/ragchat-api).
It's build to fulfill the style requirements of the University of Hamburg.

# Prerequisits
- [npm](https://www.npmjs.com/) installed
- [RAGChat-API](https://gitlab.rrz.uni-hamburg.de/zbhai/ragchat-api) running & reachable
- To prevent CORS errors add the frontend address to backends .env FRONTEND_URL

# Install
```
git clone git@gitlab.rrz.uni-hamburg.de:zbhai/ragchat-api.git
cd ragchat-api
npm i
cp ./.env.template.local ./.env.development.local
cp ./.env.template.local ./.env.production.local
# fill envs with production and/or devel values
```


# Sources
- [RAGChat-API](https://gitlab.rrz.uni-hamburg.de/zbhai/ragchat-api)
- [PM2](https://pm2.keymetrics.io/)

# Routes
- [x] AUTH
  - [x] register
  - [x] confirm register
  - [x] resend register token
  - [x] login
  - [x] renew JWT
  - [x] logout
  - [x] request password reset
  - [x] password reset
- [x] AI
  - [x] status
  - [x] get models
  - [x] get model
  - [x] install model [admin only]
  - [x] delete model [admin only]
  - [x] chat
  - [x] list chats
- [x] EMBEDDINGS
  - [x] status
  - [x] delete vector db [admin only]
  - [x] update embeddings [admin only]

# Roadmap
- [ ] fix errors
  - [ ] check width of label & submit on cleanLayout