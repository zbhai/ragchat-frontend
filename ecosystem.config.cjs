module.exports = {
  apps: [{
    name: "RAGChat-Frontend",
    script: "npm",
    args: "run dev",
    watch: false,
    error_file: 'logs/error.log',
    out_file: 'logs/output.log'
  }]
};