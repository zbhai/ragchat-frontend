import React from 'react';
import { Button } from '../ui/button';
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle
} from "/src/components/ui/dialog";

function ConfirmBox({ confirmDialog, closeDialog, item, handleProceed, ...props }) {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <Dialog open={confirmDialog.open} onOpenChange={closeDialog}>
      <DialogContent className="sm:max-w-md">
        <DialogHeader>
          <DialogTitle>Remove</DialogTitle>
          <DialogDescription>
            You are about to remove the following item:<br />
            <span className='text-lg'>{JSON.stringify(confirmDialog.displayName)}</span>
          </DialogDescription>
        </DialogHeader>
        Proceed?
        <DialogFooter className="sm:justify-start gap-2">
          <DialogClose asChild>
            <Button>
              Abort
            </Button>
          </DialogClose>
          <DialogClose asChild>
            <Button variant="destructive" onClick={handleProceed}>
              remove
            </Button>
          </DialogClose>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}

export default React.memo(ConfirmBox);