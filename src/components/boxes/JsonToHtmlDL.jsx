import React from 'react';

function JsonToHtmlDL({ jsonContent }) {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################
  const renderDL = (obj) => {
    if (obj === null || typeof obj !== 'object') {
      return null;
    }

    return (
      <dl className='ml-2'>
        {Object.entries(obj).map(([key, value]) => (
          <React.Fragment key={key}>
            <dt className='text-UhhRed uppercase'>{key}</dt>
            <dd className='ml-4 whitespace-pre'>
              {typeof value === 'object' && value !== null ? renderDL(value) : (value ? value.toString() : '')}
            </dd>
          </React.Fragment>
        ))}
      </dl>
    );
  };

  // #################################
  // OUTPUT
  // #################################
  return <div>{renderDL(jsonContent)}</div>;
}

export default React.memo(JsonToHtmlDL);