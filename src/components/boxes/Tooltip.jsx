import React from 'react';
import { RiQuestionLine } from 'react-icons/ri';

function Tooltip({ children, ...props }) {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <span className='group ml-2'>
      <RiQuestionLine className='text-UhhBlue hover:cursor-help' />
      <div className='absolute invisible group-hover:visible group-hover:z-50 bg-UhhWhite border border-UhhBlue p-2'>
        {children}
      </div>
    </span>
  );
}

export default React.memo(Tooltip);