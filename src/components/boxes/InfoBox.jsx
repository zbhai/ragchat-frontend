import React from 'react';
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "/src/components/ui/dialog";
import { Button } from '../ui/button';
import JsonToHtmlDL from './JsonToHtmlDL';

function InfoBox({ infoDialog, closeDialog, item, ...props }) {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <Dialog open={infoDialog.open} onOpenChange={closeDialog}>
      <DialogContent className="max-w-[95%] grid-rows-[auto_minmax(0,1fr)_auto] p-0 max-h-[90dvh]">
        <DialogHeader className='p-6 pb-0'>
          <DialogTitle>{infoDialog.title}</DialogTitle>
          <DialogDescription>
            {infoDialog.description}
          </DialogDescription>
        </DialogHeader>
        <div className="grid gap-4 py-4 overflow-y-auto px-6">
          <div className="flex flex-col justify-between h-[300dvh]">
            <JsonToHtmlDL jsonContent={infoDialog.body} />
          </div>
        </div>
        <DialogFooter className="sm:justify-start gap-2 p-6 pt-0">
          <DialogClose asChild>
            <Button>
              close
            </Button>
          </DialogClose>
        </DialogFooter>
      </DialogContent>
    </Dialog >
  );
}

export default React.memo(InfoBox);