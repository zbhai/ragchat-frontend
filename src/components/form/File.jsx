import React, { useEffect, useId, useState } from 'react';
import { useFormContext, useWatch } from 'react-hook-form';
import { RiEdit2Line, RiDeleteBinLine } from 'react-icons/ri';
import api from '../../utils/AxiosConfig';
import { mergeBackendValidation } from '../../utils/ErrorHandling';
import { capitalizeFirstLetter } from '../../utils';

function File({ name, currentfile, title, deletefnc, ...props }) {
  // #################################
  // HOOKS
  // #################################
  // ### Prepare form
  const {
    register,
    control,
    setError,
    formState: { errors }
  } = useFormContext();

  const id = useId();

  const watchFile = useWatch({
    control,
    name
  });

  // ### PREVIEW
  const [mimetype, setMimetype] = useState();
  const [blob, setBlob] = useState();


  //### FILE PREVIEW
  useEffect(() => {
    if (watchFile) {
      const reader = new FileReader();
      reader.onloadend = () => {
        // set mimetype to decide which media element shall be rendered
        setMimetype((watchFile[0].type).split('/')[0]);
        // set blob for media element
        setBlob(reader.result);
      };
      if (watchFile[0]) reader.readAsDataURL(watchFile[0]);
    };
  }, [watchFile]);

  // ### GET FILE
  useEffect(() => {
    const getFile = async () => {
      try {
        await api.post('/files/download', { path: currentfile.path }, { responseType: 'blob' }).then(result => {
          let objectURL = URL.createObjectURL(result.data);
          // set mimetype to decide which media element shall be rendered
          setMimetype((currentfile?.mimetype).split('/')[0]);
          // set blob for media element
          setBlob(objectURL);
        });
      } catch (error) {
        mergeBackendValidation(error.response.status, error.response.data, setError);
      }
    };
    getFile();
  }, [currentfile]);

  // #################################
  // FUNCTIONS
  // #################################


  // #################################
  // OUTPUT
  // #################################
  return (
    <label htmlFor={id}>
      {capitalizeFirstLetter(title)}

      <input {...register(name)}
        id={id}
        aria-invalid={errors[name] ? 'true' : 'false'}
        className='hidden aria-[invalid=true]:border-UhhRed'
        autoComplete='off'
        {...props}
      />
      <div className="relative bg-UhhWhite p-1 max-w-fit border border-UhhGrey">
        {mimetype === 'video' ?
          <video controls>
            <source src={blob} type="video/mp4" />
          </video>
          :
          <img src={blob} alt={`file for ${title}`} className="max-h-48 object-cover" title={currentfile.originalname} />
        }

        {!watchFile &&
          <div className="absolute inset-0 flex justify-evenly group items-center text-UhhWhite ">
            <span className="w-full h-full text-center flex justify-center items-center bg-UhhBlue opacity-0 group-hover:opacity-70 hover:!opacity-100 transition-all">
              <RiEdit2Line className="inset-0" title="click to select a new media" />
            </span>

            <span onClick={deletefnc} className="w-full h-full text-center flex justify-center items-center bg-UhhRed opacity-0 group-hover:opacity-70 hover:!opacity-100 transition-all">
              <RiDeleteBinLine className="inset-0 pointer-events-none" title="click to delete this media" />
            </span>
          </div>
        }

      </div>
      {watchFile && <p className="validationNote mt-2 text-UhhBlue text-xs overflow-auto">Preview only. Save to make change permanent.</p>}
      <p className="validationNote mt-2 text-UhhRed text-xs overflow-auto"></p>
    </label>
  );
}

export default React.memo(File);