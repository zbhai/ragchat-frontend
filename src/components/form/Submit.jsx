import { cva } from 'class-variance-authority';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { twMerge } from 'tailwind-merge';
import { cn } from '../../utils';

function Submit({ value, type, className, variant, size, ...props }) {
  // #################################
  // HOOKS
  // #################################
  const {
    formState: { isSubmitting, isValid }
  } = useFormContext();

  // #################################
  // FUNCTIONS
  // #################################
  // ### MERGE CLASSNAMES
  // static class names every item should have
  let staticClassName = 'inline-block w-full h-16 mb-4 box-border border border-UhhBlue leading-[4rem] text-center align-middle bg-UhhBlue border-UhhBlue text-UhhWhite font-UhhBC cursor-pointer disabled:cursor-not-allowed disabled:opacity-60 lg:min-w-xs lg:w-[calc(1/2*100%-(1*1rem/2))] xl:w-[calc((1/4*100%)-(3*1rem/4))] hover:text-UhhBlue hover:bg-UhhWhite';
  // dynamic class names depending on the type
  const typeClassName = () => {
    switch (type = 'submit') {
      default: {
        return '';
      }
    }
  };

  const buttonVariants = cva([
    "inline-block",
    "w-full",
    "mb-4",
    "box-border",
    "border",
    "border-UhhBlue",
    "text-center",
    "align-middle",
    "bg-UhhBlue",
    "border-UhhBlue",
    "text-UhhWhite",
    "font-UhhBC",
    "cursor-pointer",
    "disabled:cursor-not-allowed",
    "disabled:opacity-60",
    "lg:min-w-xs",
    "lg:w-[calc(1/2*100%-(1*1rem/2))]",
    "xl:w-[calc((1/4*100%)-(3*1rem/4))]",
    "hover:text-UhhBlue",
    "hover:bg-UhhWhite"],
    {
      variants: {
        variant: {
          default: ["border-primary",
            "bg-primary",
            "text-primary-foreground",
            "hover:bg-UhhWhite",
            "hover:text-primary"],
          destructive:
            ["border-destructive",
              "bg-destructive",
              "text-destructive-foreground",
              "hover:bg-UhhWhite",
              "hover:text-destructive"],
          outline:
            ["border",
              "border-input",
              "bg-background",
              "hover:bg-accent",
              "hover:text-accent-foreground"],
          secondary:
            ["bg-secondary",
              "text-secondary-foreground",
              "hover:bg-secondary/80"],
          ghost: ["hover:bg-accent",
            "hover:text-accent-foreground"],
          link: ["text-primary",
            "underline-offset-4",
            "hover:underline"],
        },
        size: {
          default: "h-16",
          sm: "h-12 w-fit px-4",
          lg: "h-16 px-8",
          xl: "h-20 px-12",
          icon: "h-10 w-10",
        },
      },
      defaultVariants: {
        variant: "default",
        size: "default",
      },
    }
  );

  // merge static and dynamic class names with className from props
  const mergedClassName = twMerge(staticClassName, typeClassName(), className);
  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {/* <input type="submit" className={mergedClassName} value={value} disabled={isSubmitting} /> */}
      <input type="submit" className={cn(buttonVariants({ variant, size, className }))} value={value} disabled={!isValid || isSubmitting} {...props} />
    </>
  );
}

export default React.memo(Submit);