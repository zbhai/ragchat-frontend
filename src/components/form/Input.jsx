import React, { useId } from 'react';
import { useFormContext } from 'react-hook-form';
import { twMerge } from 'tailwind-merge';
import Tooltip from '../boxes/Tooltip';
import RequiredBadge from './RequiredBadge';
import { capitalizeFirstLetter } from '../../utils';

function Input({ title, name, type, className, tooltip, ...props }) {
  // #################################
  // HOOKS
  // #################################
  const {
    register,
    formState: { errors }
  } = useFormContext();

  const id = useId();
  // #################################
  // FUNCTIONS
  // #################################
  // ### MERGE CLASSNAMES
  // static class names every item should have
  const staticClassName = 'focus:outline-none focus:border-UhhBlue block box-border h-8 px-4 border border-UhhGrey bg-UhhLightGrey w-full caret-UhhRed aria-[invalid=true]:border-UhhRed disabled:text-UhhGrey/70 disabled:cursor-not-allowed';
  // dynamic class names depending on the type
  const typeClassName = () => {
    switch (type) {
      case 'file': {
        return 'file:text-xs file:py-2 file:border-0 file:bg-UhhBlue file:text-UhhWhite hover:cursor-pointer hover:file:cursor-pointer hover:file:text-UhhBlue hover:file:bg-UhhWhite pl-0';
      }
      default: {
        return ' ';
      }
    }
  };
  // merge static and dynamic class names with className from props
  const mergedClassName = twMerge(staticClassName, className);

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      <label htmlFor={id}>
        {props.required && <RequiredBadge />}
        {title && capitalizeFirstLetter(title)}
        {tooltip && <Tooltip>{tooltip}</Tooltip>}

        <input {...register(name)}
          type={type}
          id={id}
          aria-invalid={errors[name] ? 'true' : 'false'}
          className={mergedClassName}
          autoComplete={type === 'password' ? 'off' : 'on'}
          {...props}
        />
        <p className="validationNote mt-2 text-UhhRed text-xs overflow-auto">{errors[name]?.message}</p>
      </label>
    </>
  );
};

export default React.memo(Input);