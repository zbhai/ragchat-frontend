import React, { useId } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { twMerge } from 'tailwind-merge';
import { capitalizeFirstLetter } from '../../utils';
import Tooltip from '../boxes/Tooltip';
import RequiredBadge from './RequiredBadge';


function Select({ options, name, title, defaultValue, className, tooltip, type, ...props }) {
  // #################################
  // HOOKS
  // #################################
  const {
    control,
    formState: { errors }
  } = useFormContext();

  // GENERATE RANDOM UUID
  const id = useId();

  // #################################
  // FUNCTIONS
  // #################################
  // ### MERGE CLASSNAMES
  // static class names every item should have
  const staticClassName = 'focus:outline-none focus:border-UhhBlue block box-border h-8 px-4 border border-UhhGrey bg-UhhLightGrey w-full aria-[invalid=true]:border-UhhRed';
  // dynamic class names depending on the type
  const typeClassName = () => {
    switch (type) {
      default: {
        return '';
      }
    }
  };
  // merge static and dynamic class names with className from props
  const mergedClassName = twMerge(staticClassName, typeClassName(), className);

  // #################################
  // OUTPUT
  // #################################
  return (
    <label htmlFor={id}>
      {props.required && <RequiredBadge />}
      {title && capitalizeFirstLetter(title)}
      {tooltip && <Tooltip>{tooltip}</Tooltip>}

      <Controller
        render={
          ({ field }) => <select className={mergedClassName} {...field} {...props}>
            <option key={`option-${id}`} value='' disabled>-- Select --</option>
            {options.map(option => (
              <option key={`option-${option._id}`} value={option._id}>
                {option.title}
              </option>
            ))}
          </select>
        }
        control={control}
        name={name}
        defaultValue={defaultValue}
        id={id}
      />
      <p className="validationNote mt-2 text-UhhRed text-xs overflow-auto">{errors[name]?.message}</p>
    </label>
  );
}

export default React.memo(Select);