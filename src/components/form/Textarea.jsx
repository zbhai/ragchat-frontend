import React, { useId } from 'react';
import { useFormContext } from 'react-hook-form';
import { twMerge } from 'tailwind-merge';
import Tooltip from '../boxes/Tooltip';
import RequiredBadge from './RequiredBadge';
import { capitalizeFirstLetter } from '../../utils';


function Textarea({ title, name, className, type, tooltip, children, ...props }) {
  // #################################
  // HOOKS
  // #################################
  const {
    register,
    formState: { errors }
  } = useFormContext();

  const id = useId();
  // #################################
  // FUNCTIONS
  // #################################
  // ### MERGE CLASSNAMES
  // static class names every item should have
  const staticClassName = 'h-24 focus:outline-none focus:border-UhhBlue block box-border h-16 py-2 px-4 border border-UhhGrey bg-UhhLightGrey w-full caret-UhhRed aria-[invalid=true]:border-UhhRed text-sm sm:text-base';
  // dynamic class names depending on the type
  const typeClassName = () => {
    switch (type) {
      default: {
        return '';
      }
    }
  };
  // merge static and dynamic class names with className from props
  const mergedClassName = twMerge(staticClassName, typeClassName(), className);

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      <label htmlFor={id}>
        {props.required && <RequiredBadge />}
        {capitalizeFirstLetter(title)}
        {tooltip && <Tooltip>{tooltip}</Tooltip>}

        <div className="inline-flex w-full">
          <textarea {...register(name)}
            id={id}
            aria-invalid={errors[name] ? 'true' : 'false'}
            className={mergedClassName}
            autoComplete='off'
            {...props}>
          </textarea>

          {children}
        </div>
        <p className="validationNote mt-2 text-UhhRed text-xs overflow-auto">{errors[name]?.message}</p>
      </label>
    </>
  );
}

export default React.memo(Textarea);