import React from 'react';

function RequiredBadge() {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <span className="text-UhhRed leading-4" title="This field is required">* </span>
  );
}

export default React.memo(RequiredBadge);