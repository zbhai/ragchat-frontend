import React, { useEffect, useState } from 'react';
import Textarea from './Textarea';
import { Button } from '../ui/button';
import ConfirmBox from '../boxes/ConfirmBox';
import CustomTable from '../table/CustomTable';
import { useFieldArray } from 'react-hook-form';


function FlatListEdit({ initialItems = [], fieldName = '', tooltip = '', columns = [], methods, validator, keyName, confirmDialog, setConfirmDialog }) {
  // #################################
  // HOOKS
  // #################################

  // store current data in state
  const [data, setData] = useState(initialItems);
  // set first data set
  useEffect(() => {
    // initialize table state
    setData(initialItems);
    replace(initialItems);
  }, [initialItems]);

  // ### USE FIELD ARRAY TO STORE ITEMS
  const { fields, remove, replace } = useFieldArray({
    control: methods.control,
    name: fieldName,
  });

  // #################################
  // FUNCTIONS
  // #################################
  // ### ADD ITEMS FROM TEXTAREA
  const handleAddItems = (e) => {
    // clear error on input field
    methods.clearErrors('addItem');
    // fetch values from textarea and split new lines & csv into array
    const addItems = methods.getValues('addItem').split(/[\n,]/);
    // return on empty input
    if (addItems.length === 1 && addItems[0] === '') return;


    // get valid entries
    // trim spaces from each input
    addItems.forEach((input, idx) => addItems[idx] = input.trim());
    // filter array for valid entries using zod
    const validInputs = addItems.filter((input, idx) => {
      const isValid = validator(input);
      return isValid.success ?? keyName;
    });
    // exit if no valid items found
    if (validInputs.length === 0) return;

    // handle invalid inputs
    // strip off valid entries from input to get remaining invalid inputs
    const invalidInputs = addItems.filter(keyName => !validInputs.includes(keyName));
    // set input value to remaining invalid entries
    methods.setValue('addItem', invalidInputs.join('\n'));
    if (invalidInputs.length > 0) {
      // set error message on input
      methods.setError('addItem', { message: 'invalid entries remaining' });
    }


    // handle valid inputs
    // get existing items from fieldarray
    const extistingItems = methods.getValues(fieldName) || [];

    // flatten fieldarray to array of strings only to merge with new inputs
    let flatArray = extistingItems.map(field => field[keyName]);
    // add valid inputs to existing items avoiding douplettes
    flatArray = ([... new Set([...flatArray, ...validInputs])]);
    // sort & convert flat array back into object
    const wholeItemsObject = flatArray.sort().map(input => ({ [keyName]: input }));
    // replace whole table data (invisible)
    setData(wholeItemsObject);
    // replace whole fieldarray, which will be submitted to backend
    replace(wholeItemsObject);
    // set focus back to input field, which is only used for display
    methods.setFocus('addItem');
  };


  // ### REMOVE ITEM FROM ARRAY
  const handleRemoveItem = (idx) => {
    // delete from fieldarray, which will be submitted to backend
    remove(idx);
    // delete from tableData, which is only used for display
    setData(methods.getValues(fieldName));
  };

  // #################################
  // OUTPUT
  // #################################

  return (
    <>
      {/* add Owner */}
      <Textarea name='addItem' title={fieldName} tooltip={tooltip}>
        <Button type='button' variant="default" onClick={handleAddItems} className='px-4 h-16 self-center'>add</Button>
      </Textarea>
      {/* List */}
      <CustomTable columns={columns} data={data} />

      {fields && fields.map((item, idx) => {
        return <div key={idx}>
          <input className='w-full h-full' type='hidden' key={item.id} {...methods.register(`${fieldName}.${idx}.${keyName}`)} disabled={true} />
        </div>;
      })}

      {/* confirmDialog */}
      <ConfirmBox confirmDialog={confirmDialog} closeDialog={() => setConfirmDialog({ ...confirmDialog, open: false })} handleProceed={() => { handleRemoveItem(confirmDialog.idToDelete); }} />
    </>
  );
}

export default FlatListEdit;