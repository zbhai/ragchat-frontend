import React from 'react';
import { twMerge } from 'tailwind-merge';

function Fieldset({ title, className, type, children, ...props }) {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################
  // ### MERGE CLASSNAMES
  // static class names every item should have
  const staticClassName = 'mb-8 p-1 border-t border-UhhLightGray';
  // dynamic class names depending on the type
  const typeClassName = () => {
    switch (type) {
      default: {
        return '';
      }
    }
  };
  // merge static and dynamic class names with className from props
  const mergedClassName = twMerge(staticClassName, typeClassName(), className);

  // #################################
  // OUTPUT
  // #################################
  return (
    <fieldset className={mergedClassName}>
      <legend className='ml-2 text-xs text-UhhGrey'>{title}</legend>
      {children}
    </fieldset>
  );
}

export default React.memo(Fieldset);