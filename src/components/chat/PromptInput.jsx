import { zodResolver } from '@hookform/resolvers/zod';
import React, { useEffect } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { RiLoopRightFill, RiSendPlane2Line } from 'react-icons/ri';
import { z } from 'zod';

import { useChat } from '../../contexts/Chat/ChatState';
import api from '../../utils/AxiosConfig';
import { mergeBackendValidation } from '../../utils/ErrorHandling';
import Input from '../form/Input';
import Select from '../form/Select';

function PromptInput() {
  // #################################
  // VALIDATION SCHEMA
  // #################################
  const schema = z.object({
    input: z.string().min(1),
    model: z.string().min(1)
  });

  // #################################
  // HOOKS
  // #################################
  // ### CONNECT CONTEXT
  const { fetchAllModels, currentChatId, availableModels, updateChatHistory, fetchAllChats } = useChat();

  // ### PREPARE FORM
  const methods = useForm({
    resolver: zodResolver(schema),
    mode: 'onSubmit',
    defaultValues: {
      input: '',
      model: ''
    }
  });

  // ### FETCH MODELS;
  useEffect(() => {
    // ### on run exec this code
    const controller = new AbortController();

    const getModels = async () => {
      try {
        // fetch all chats
        await fetchAllModels();
      } catch (error) {
        mergeBackendValidation(error.response.status, error.response.data);
      }
    };
    getModels();

    // ### return will be executed on unmounting this component
    return () => {
      // on unmount abort request
      controller.abort();
    };
  }, []);

  // #################################
  // FUNCTIONS
  // #################################
  // ### HANDLE SUBMITTING FORM
  async function handleSendForm(inputs) {
    // invoke chatID if available
    if (currentChatId) { inputs.chatId = currentChatId; };
    // send data to api
    try {
      // send input to api
      const result = await api.post('/ai/chat', inputs);
      if (!currentChatId) fetchAllChats(result.data.chat.id);

      // update chat history
      updateChatHistory(result.data.chat.id, result.data.chat.chatHistory);
      // selectChat(currentChatId);
      // clear input field
      methods.resetField('input');

    } catch (error) {
      console.error(error);
      // merge front & backend validation errors
      mergeBackendValidation(error.response.status, error.response.data, methods.setError);
    }


  }

  // TODO width of input field on large screens should increase
  // #################################
  // OUTPUT
  // #################################
  return (
    <div className="row-start-3 p-3 border-t-2 border-UhhGrey text-center">
      <FormProvider {...methods} >
        <form onSubmit={methods.handleSubmit(handleSendForm)}>
          <div className="flex justify-center content-center h-14 relative">
            {methods.formState.isSubmitting && <div className='absolute bg-white bg-opacity-60 z-10 h-full w-full flex items-center justify-center text-2xl'><RiLoopRightFill className='animate-spin' /></div>}

            <Select name="model" options={availableModels} />
            <Input name="input" type="text" placeholder="Type a message" className="block h-8" />

            <button type="submit" className="h-8 justify-center items-center bg-UhhBlue text-UhhWhite p-1 text-xs">
              <RiSendPlane2Line />
            </button>

          </div>
        </form>
      </FormProvider>
    </div>
  );
}

export default React.memo(PromptInput);