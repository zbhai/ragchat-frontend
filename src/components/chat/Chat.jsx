import React from 'react';
import { useChat } from '../../contexts/Chat/ChatState';


const Chat = ({ id, title, time }) => {
  // #################################
  // HOOKS
  // #################################
  // ### CONNECT CONTEXT
  const { currentChatId, selectChat } = useChat();


  // #################################
  // FUNCTIONS
  // #################################
  // ### mark active chat
  const active = (currentChatId === id ? 'bg-gray-200' : 'bg-white');

  // #################################
  // OUTPUT
  // #################################
  return (
    <button onClick={() => { selectChat(id); }} className={`block p-1 hover:bg-gray-200 m-1 rounded-md ${active}`} title={title}>
      <div className={'flex items-center p-2  cursor-pointer  '}>
        <div className="flex-grow p-2">
          <div className="flex justify-between text-md">

            <div className="text-xs text-gray-400 dark:text-gray-300">{time}</div>
          </div>
          <div className="text-sm text-gray-500 dark:text-gray-400  w-40 truncate">
            {title}
          </div>
        </div>
      </div>
    </button>
  );
};

export default Chat;