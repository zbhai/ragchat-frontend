import React from 'react';

function Message({ chat }) {
  // #################################
  // HOOKS
  // #################################


  // #################################
  // FUNCTIONS
  // #################################
  // TODO: use tailwind merge
  // AI css
  let tilePosition = '';
  let tileColor = 'bg-UhhGrey';
  let tileBorder = 'rounded-bl-none';
  let tileMargin = 'mr-8';
  let senderClasses = 'text-UhhLightGrey';
  let messageClasses = 'text-UhhWhite';

  // user css
  if (chat.type === 'human') {
    tilePosition = 'justify-end';
    tileColor = 'bg-UhhLightGrey';
    tileBorder = 'rounded-br-none';
    tileMargin = 'ml-8';
    senderClasses = 'text-UhhGrey';
    messageClasses = 'text-UhhGrey';
  }

  const renderSource = () => {
    if (chat.type === 'human') return '';
    if (!chat.data.source) return 'pretrained';

    const file = chat.data.source.metadata.source.split('/').reverse()[0];
    const posFrom = chat.data.source.metadata.loc.lines.from;
    const posTo = chat.data.source.metadata.loc.lines.to;


    const source = `${file}:${posFrom}-${posTo}`;
    return <span className='group'>{source}
      <div className='absolute invisible text-UhhGrey group-hover:visible group-hover:z-50 bg-UhhWhite border border-UhhBlue p-2 whitespace-pre-line'>
        {chat.data.source.pageContent}
      </div>
    </span>;
  };

  // #################################
  // OUTPUT
  // #################################
  return (
    <div className={`flex ${tilePosition}`} >
      <div className={`p-3 mx-3 my-1 rounded-2xl ${tileBorder} ${tileColor} ${tileMargin}`}>
        <div className={`text-xs flex justify-between ${senderClasses}`} >
          <div>{chat.type}</div>
          <div>{renderSource()}</div>
        </div>
        <div className={`${messageClasses} whitespace-pre-line`}>
          {chat.data.content}
        </div>

      </div>
    </div>
  );
}

export default React.memo(Message);