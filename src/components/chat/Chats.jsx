import React, { useEffect, useState } from 'react';
import { RiAddCircleLine, RiArrowLeftCircleLine, RiArrowRightCircleLine } from 'react-icons/ri';
import { useNavigate, useParams } from 'react-router-dom';
import { useChat } from '../../contexts/Chat/ChatState';
import { mergeBackendValidation } from '../../utils/ErrorHandling';
import Heading from '../font/Heading';
import Chat from './Chat';

const Chats = () => {
  // #################################
  // HOOKS
  // #################################
  // ### showSidebar 
  const initialShowSidebar = true;
  const [showSidebar, setShowSidebar] = useState(initialShowSidebar);

  // FETCH CHAT ID FROM URL
  const { id } = useParams();

  // ### CONNECT CONTEXT
  const { fetchAllChats, chatHeadings, currentChatId, selectChat } = useChat();

  // ### FETCH CHATS;
  useEffect(() => {
    // ### on run exec this code
    const controller = new AbortController();

    const getChats = async () => {
      try {
        // fetch all chats
        await fetchAllChats(id || null);
      } catch (error) {
        console.error(error);
        mergeBackendValidation(error.response.status, error.response.data);
      }
    };
    getChats();

    // ### return will be executed on unmounting this component
    return () => {
      // on unmount abort request
      controller.abort();
    };
  }, [currentChatId]);

  // ### ENABLE REDIRECTIONS
  const redirect = useNavigate();
  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <div className="row-start-2 row-span-2 border-r-2 border-UhhGrey flex flex-col">
      <Heading level="6" className="text-center mx-2">Recent</Heading>

      {<button onClick={() => { selectChat(null); return redirect('/onboarding'); }} disabled={currentChatId ? false : true} className='text-UhhBlue disabled:text-UhhLightBlue' title='start a new chat'><RiAddCircleLine /></button>}

      <div className="p-1">
        {showSidebar &&
          chatHeadings.map((chat, index) => (
            <Chat
              key={chat.id}
              id={chat.id}
              title={chat.title}
              time={new Intl.DateTimeFormat(import.meta.env.VITE_LOCALE).format(new Date(chat.createdAt))}
            />
          ))
        }
      </div>
      <div className="mt-auto mb-8 flex justify-end text text-4xl" title='show / hide Chat'>
        <button onClick={() => setShowSidebar(!showSidebar)}>
          {showSidebar ? <RiArrowLeftCircleLine /> : <RiArrowRightCircleLine />}

        </button>
      </div>
    </div >
  );
};

export default Chats;