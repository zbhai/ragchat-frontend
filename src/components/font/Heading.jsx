import React from 'react';
import { twMerge } from 'tailwind-merge';

function Heading({ level = 1, children, className, ...props }) {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################
  const Tag = `h${level}`;

  // ### MERGE CLASSNAMES
  // static class names every item should have
  const staticClassName = '';
  // dynamic class names depending on the type
  const typeClassName = () => {
    switch (level) {
      case '1': {
        return 'font-UhhBC text-4xl my-8';
      }
      case '2': {
        return 'font-UhhSLC text-3xl leading-7';
      }
      case '3': {
        return 'font-UhhSLC text-3xl';
      }
      case '4': {
        return 'font-UhhB text-2xl py-6';
      }
      case '5': {
        return 'font-UhhSLC text-lg';
      }
      case '6': {
        return 'font-UhhSLC';
      }
    }
  };
  // merge static and dynamic class names with className from props
  const mergedClassName = twMerge(staticClassName, typeClassName(), className);


  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      <Tag className={mergedClassName}>{children}</Tag>
    </>
  );
}

export default React.memo(Heading);