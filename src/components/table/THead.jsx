import React, { Fragment } from 'react';
import { TableHead, TableHeader, TableRow } from '../ui/table';
import { RiLineHeight, RiSearchLine, RiSortAsc, RiSortDesc } from 'react-icons/ri';



function THead({ table, setColumnFilters, ...props }) {
  // #################################
  // HOOKS
  // #################################


  // #################################
  // FUNCTIONS
  // #################################
  // ### RENDER TITLE CELL
  const renderTitleCell = (header) => {
    if (header.column.getCanSort()) {
      // return title cell with sorting icon
      return (
        <TableHead key={header.id} className={`${header.column.columnDef.meta?.headerClassName} cursor-pointer group bg-UhhWhite bg-clip-padding`} onClick={header.column.getToggleSortingHandler()}>
          <span className='flex justify-between items-center pointer-events-none'>
            {header.column.columnDef.header}
            {renderOrderIcon(header.column.getIsSorted())}
          </span>
        </TableHead>);
    } else {
      // prevent error on columns without header
      const title = (typeof header.column.columnDef.header === 'string') ? header.column.columnDef.header : '';
      return <TableHead key={header.id} className={`${header.column.columnDef.meta?.headerClassName} cursor-pointer group bg-UhhWhite bg-clip-padding`}>{title}</TableHead>;
    }
  };

  // ### RENDER ORDER ICON
  const renderOrderIcon = (sorted) => {
    // check if this is the ordered column
    switch (sorted) {
      case 'asc':
        return <RiSortDesc className='pointer-events-none' />;
      case 'desc':
        return <RiSortAsc className='pointer-events-none' />;
      default:
        // if it's not the ordered column, return a different icon
        return <span className='text-muted group-hover:text-muted-foreground'>
          <RiLineHeight className='pointer-events-none rotate-180' />
        </span>;
    }
  };


  // ### GET FILTERABLE COLUMNS
  const filterableCols = () => {
    return table.getAllColumns().filter(column => column.getCanFilter() || false);
  };

  // ### RENDER FILTER CELL
  const renderFilterCell = (header) => {
    if (header.column.columnDef.enableColumnFilter) {
      return <TableHead key={header.id}>
        <span className='flex items-center'>
          <RiSearchLine className='text-UhhBlue' />
          <input type="search" name={header.column.columnDef.accessorKey} placeholder={`filter ${header.column.columnDef.header}`}
            className='p-1 w-full outline-none border-none bg-transparent focus:ring-0 font-UhhR text-xs text-UhhRed' onChange={(e) => { onFilterChange(e.target.name, e.target.value); }} />
        </span>
      </TableHead>;
    }
    // if it's not filterable, return an empty cell
    return <TableHead key={header.id}></TableHead>;
  };

  // ### CHANGE FILTER
  const onFilterChange = (id, value) => setColumnFilters(
    prev => prev.filter(f => f.id !== id).concat({ id, value })
  );

  // #################################
  // OUTPUT
  // #################################
  return (
    <TableHeader className='sticky top-0'>
      {/* TITLES */}
      {table.getHeaderGroups().map(headerGroup => {
        return (
          <Fragment key={headerGroup.id}>
            <TableRow>
              {headerGroup.headers.map(header => {
                return renderTitleCell(header);
              })}
            </TableRow>
            {/* FILTERS */}
            {filterableCols().length > 0 && <TableRow>
              {headerGroup.headers.map((header) => renderFilterCell(header))}
            </TableRow>}
          </Fragment>
        );
      })}
    </TableHeader>
  );
}

export default THead;