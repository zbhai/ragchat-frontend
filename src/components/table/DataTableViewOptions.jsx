import React, { useState } from 'react';
import { Button } from '../ui/button';
import {
  DropdownMenu,
  DropdownMenuCheckboxItem,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "/src/components/ui/dropdown-menu";

function DataTableViewOptions({ table }) {
  // #################################
  // HOOKS
  // #################################
  // force rerender to toggle visibility in dropdown
  const [rerender, setRerender] = useState(0);

  // #################################
  // FUNCTIONS
  // #################################
  // ### GET HIDEABLE COLUMNS
  const hideableCols = () => {
    return table.getAllColumns().filter(column => typeof column.accessorFn !== "undefined" && column.getCanHide());
  };
  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {hideableCols().length > 0 &&
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button variant="outline" className="ml-auto">
              Columns
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end">
            {hideableCols()
              .map((column) => {
                return (
                  <DropdownMenuCheckboxItem
                    key={column.id}
                    className="capitalize"
                    checked={column.getIsVisible()}
                    onCheckedChange={(value) => {
                      setRerender(rerender + 1);
                      column.toggleVisibility(!!value);
                    }
                    }
                  >
                    {column.id}
                  </DropdownMenuCheckboxItem>
                );
              })}
          </DropdownMenuContent>
        </DropdownMenu>
      }
    </>
  );
}

export default React.memo(DataTableViewOptions);