import React from 'react';
import { TableBody, TableCell, TableRow } from '../ui/table';
import { flexRender } from '@tanstack/react-table';

function TBody({ table }) {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <TableBody>
      {table.getRowModel().rows.map(row => {
        return (
          <TableRow key={row.id}>
            {row.getVisibleCells().map(cell => {
              return (
                <TableCell key={cell.id} className={cell.column.columnDef.meta?.cellClassName}>
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </TableCell>
              );
            })}
          </TableRow>
        );
      })}
    </TableBody>
  );
}

export default TBody;