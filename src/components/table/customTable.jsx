import React, { useState } from 'react';
import { getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from '@tanstack/react-table';
import { Table } from "/src/components/ui/table";
import Heading from '../font/Heading';
import DataTableViewOptions from './DataTableViewOptions';
import THead from './THead';
import Pagination from './Pagination';
import TBody from './TBody';


function CustomTable({ columns = [], data = {}, title = '' }) {
  // #################################
  // HOOKS
  // #################################

  // ### INIT FILTERS
  const [columnFilters, setColumnFilters] = useState([]);
  // ### INIT VISIBILITY
  const [columnVisibility, setColumnVisibility] = useState({});
  // ### INIT TABLE
  const table = useReactTable({
    data,
    columns,
    state: {
      columnFilters,
      columnVisibility
    },
    getFilteredRowModel: getFilteredRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getCoreRowModel: getCoreRowModel(),
    onColumnVisibilityChange: setColumnVisibility,
    // define custom filter functions
    filterFns: {
      regex: (rows, columnIds, filterValue) => {
        try {
          const regex = new RegExp(filterValue, 'i');
          return rows.getValue(columnIds).toString().match(regex) ? true : false;
        } catch (error) {
          console.error(error);
          return false;
        }
      }
    }
  });

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {/* title line incl.  */}
      <Heading level="4" className='flex justify between'>
        {title}
        {/* HIDE COLUMNS */}
        <DataTableViewOptions table={table} />
      </Heading>
      <Table className='w-full border-collapse table-auto border border-UhhLightGrey'>
        {/* HEADER */}
        <THead table={table} setColumnFilters={setColumnFilters} />
        {/* BODY */}
        {table.getRowModel().rows.length > 0 && <TBody table={table} />}
      </Table>
      {table.getRowModel().rows.length === 0 && <div className='text-center'>No matching data found</div>}

      {/* FOOTER */}
      {table.getRowModel().rows.length > 0 && <Pagination table={table} />}

    </>
  );
};

export default React.memo(CustomTable);