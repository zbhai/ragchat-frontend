import React from 'react';
import { Button } from '../ui/button';
import { RiSkipLeftLine, RiSkipRightLine } from 'react-icons/ri';

function Pagination({ table }) {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <div className="flex items-center justify-between">
      {/* ITEM COUNT */}
      <span className='text-xs'>{table.getFilteredRowModel().rows.length} Items</span>
      {/* PAGE COUNT */}
      <span className='text-xs'>Page {table.getState().pagination.pageIndex + 1} of {table.getPageCount()}</span>
      {/* PAGINATION */}
      <span className='flex items-center justify-end space-x-2 py-4'>
        <Button
          type="button"
          variant="outline"
          size="sm"
          onClick={() => table.previousPage()}
          disabled={!table.getCanPreviousPage()}
        >
          <RiSkipLeftLine />
        </Button>

        <Button
          type="button"
          variant="outline"
          size="sm"
          onClick={() => table.nextPage()}
          disabled={!table.getCanNextPage()}
        >
          <RiSkipRightLine />
        </Button>
      </span>
    </div>
  );
}

export default Pagination;