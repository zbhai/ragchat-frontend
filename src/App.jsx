import { HelmetProvider } from 'react-helmet-async';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import AuthState from "./contexts/Auth/AuthState";
import { sitemap } from "./routes/Sitemap";


function App() {
  let pages = createBrowserRouter(sitemap);

  return (
    <HelmetProvider>
      <AuthState>
        <RouterProvider router={pages} />
      </AuthState>
    </HelmetProvider>

  );
}

export default App;
