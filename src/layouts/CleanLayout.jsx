import React, { useEffect } from 'react';
import { Outlet } from 'react-router-dom';
import Header from './partials/Header';
import { ToastContainer } from 'react-toastify';


function CleanLayout() {
  // #################################
  // HOOKS
  // #################################
  // SET BODY CLASSES
  useEffect(() => {
    // ### on run exec this code
    document.body.className = '';
    document.body.classList.add('overflow-x-hidden', 'h-full', 'flex', 'justify-center', 'relative');

    document.getElementById('root').classList.remove('grid-rows-[auto_1fr_auto]', 'min-h-full', 'w-screen', 'max-h-full', 'sm:grid-rows-[auto_auto_auto_1fr]');
    document.getElementById('root').classList.add('grid-rows-[auto_1fr]', 'h-full');
  }, []);

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      <div className="grid grid-cols-1 grid-rows-[auto_1fr] h-full min-w-xs text-UhhGrey">
        <Header layout="clean" />
        <main role="main">
          <ToastContainer />
          <div className="box-border overflow-x-hidden overflow-y-auto px-4 mt-2 ">
            <div>
              <Outlet />
            </div>
          </div>
        </main>
      </div>

    </>
  );
}

export default React.memo(CleanLayout);