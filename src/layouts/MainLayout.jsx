import React, { useEffect, useState } from 'react';
import { Outlet } from 'react-router-dom';
import Header from './partials/Header';
import Subheader from './partials/subheader/Subheader';
import Navbar from './partials/navbar/Navbar';
import { ToastContainer } from 'react-toastify';


function MainLayout() {
  // #################################
  // HOOKS
  // #################################
  // ### SET BODY CLASSES
  useEffect(() => {
    // ### on run exec this code
    document.body.className = '';
    document.body.classList.add('overflow-x-hidden', 'h-full');

    document.getElementById('root').classList.remove('grid-rows-[auto_1fr]', 'h-full');
    document.getElementById('root').classList.add('grid-rows-[auto_1fr]', 'min-h-full', 'w-screen', 'max-h-full', 'sm:grid-rows-[auto_auto_auto_1fr]');
  }, []);
  // ### CHECK MOBILE MENU STATE
  const [showMobileNav, setShowMobileNav] = useState(false);

  // #################################
  // FUNCTIONS
  // #################################
  const toggleMobileNav = (toggle) => {
    setShowMobileNav(!showMobileNav);
  };

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      <Header showMobileNav={showMobileNav} toggleMobileNav={toggleMobileNav} />
      <Subheader />
      <main role="main" className="relative row-start-2 col-span-full flex sm:justify-center overflow-y-auto sm:row-start-4">
        <ToastContainer />
        <div className="box-border px-4 mt-2 container">
          <Outlet />
        </div>
      </main>
      <Navbar showMobileNav={showMobileNav} toggleMobileNav={toggleMobileNav} />
    </>
  );
}

export default React.memo(MainLayout);