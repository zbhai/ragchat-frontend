import React from 'react';
import Breadcrumbs from './Breadcrumbs';

function Subheader() {
  // #################################
  // HOOKS
  // #################################


  // #################################
  // FUNCTIONS
  // #################################


  // #################################
  // OUTPUT
  // #################################
  return (
    <div className='group flex justify-center  box-border shadow-md z-20  h-0 open:h-10 sm:h-10 transform transition-all overflow-hidden row-start-2 sm:row-start-3 col-span-full text-sm bg-UhhWhite'>
      <div className="container flex justify-center flex-wrap">
        <Breadcrumbs />
      </div>
    </div>
  );
}

export default React.memo(Subheader);