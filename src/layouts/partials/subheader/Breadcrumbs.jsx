import React from 'react';
import { RiArrowDropRightLine, RiHome2Line } from 'react-icons/ri';
import { useMatches } from 'react-router-dom';

function Breadcrumbs() {
  // #################################
  // HOOKS
  // #################################
  let matches = useMatches();

  // #################################
  // FUNCTIONS
  // #################################
  let crumbs = matches
    // get rid of any matches that don't have handle and crumb
    .filter((match) => Boolean(match.handle?.crumb));

  // #################################
  // OUTPUT
  // #################################
  return (

    <div className="h-0 overflow-hidden sm:h-6 px-4 my-2 container sm:flex justify-between">
      <ul className="flex">
        {
          // map crumbs into an array of elements, passing the loader data to each one
          // TODO: find out how to render result of .map, then move into functions-section and render ${crumbs} in here
          crumbs.map((match, idx) =>
            <li key={`bc-${idx}`}>
              {/* show home icon on first item, others will display split img */}
              {idx === 0 ? <RiHome2Line className='text-xl mr-1' /> : <RiArrowDropRightLine className='text-2xl mx-4' />}
              {/* render handle.crumb callback */}
              {match.handle.crumb(match.data)}
            </li>)
        }
      </ul>
    </div>
  );
}

export default React.memo(Breadcrumbs);;