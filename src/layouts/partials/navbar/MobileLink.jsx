import React, { useEffect, useState } from 'react';
import { RiArrowLeftSLine, RiArrowRightSLine } from 'react-icons/ri';
import { Link } from 'react-router-dom';

function MobileLink({ to, children, mobileNavState, dispatchMobileNavState, childNode, level, toggleMobileNav }) {
  // #################################
  // HOOKS
  // #################################

  // ### HAS VALID CHILDS
  const [hasValidChilds, setHasValidChilds] = useState(false);
  useEffect(() => {
    // only nodes with children
    if (childNode?.children) {
      let validChilds = childNode.children.map(child => {
        // hidden or index childs aren't valid
        if (child.hidden || child.index) return false;
        // others are
        return true;
      }).filter(Boolean);
      // if > 0 valid childs found set to true
      if (validChilds.length) setHasValidChilds(true);
    }
  }, []);


  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      <span className={`flex border-b border-b-UhhWhite text-2xl ${level === 'pov' ? 'font-UhhBC' : null}`}>
        <span className={`w-16 py-2 px-6 text-center 
        ${level === 'pov' && mobileNavState.povParentNode[0] ?
            'cursor-pointer md:hover:bg-UhhWhite md:hover:text-UhhBlue opacity-100'
            : null}`}>
          {level === 'pov' && mobileNavState.povParentNode[0] ?
            <RiArrowLeftSLine
              onClick={() => { dispatchMobileNavState({ type: 'setPath', payload: mobileNavState.povParentNode[0]?.path }); }} />
            : null}
        </span>

        <Link
          to={to}
          onClick={() => toggleMobileNav()}
          className={`md:hover:bg-UhhWhite md:hover:text-UhhBlue py-2 pr-2 w-full 
        ${level === 'pov' ?
              'pl-20'
              : 'pl-28'}`}>
          {children}
        </Link>

        <span
          className={`w-16 py-2 px-6 text-center 
        ${level !== 'pov' && hasValidChilds ?
              'cursor-pointer md:hover:bg-UhhWhite md:hover:text-UhhBlue opacity-100'
              : null}`}>
          {level !== 'pov' && hasValidChilds ?
            <RiArrowRightSLine
              onClick={() => { dispatchMobileNavState({ type: 'setPath', payload: childNode?.path }); }} />
            : null}
        </span>
      </span>

    </>
  );
}

export default React.memo(MobileLink);