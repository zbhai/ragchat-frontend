import React from 'react';
import { Link } from 'react-router-dom';

function DesktopLink({ to, children }) {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      <Link to={to} className="peer block p-2 hover:bg-UhhWhite hover:text-UhhBlue">{children}</Link>
    </>
  );
}

export default React.memo(DesktopLink);