import React from 'react';
import { useAuth } from '../../../contexts/Auth/AuthState';
import DesktopLink from './DesktopLink';

function DesktopNav({ filteredSitemap }) {
  // #################################
  // HOOKS
  // #################################
  // ### CONNECT AUTH CONTEXT
  const { currentUser } = useAuth();

  // #################################
  // FUNCTIONS
  // #################################
  // recursively render given menu
  const renderMenu = (menu, parent = null) => {
    if (!menu) return;
    return menu.filter((item) => {
      // dont show items that are above the current user role
      if (item.gateKeeper && currentUser && currentUser.role < item.gateKeeper) return;
      return item;
    }).map((item, idx) => (
      // render menu items
      <li key={`link-${idx}`} className="relative" >
        {
          item.children?.length ? (
            <>
              <DesktopLink to={parent ? `${parent.path}/${item.path}` : item.path}>{item.title}</DesktopLink>
              <ul className="absolute z-50 h-0 overflow-y-hidden bg-UhhBlue border-UhhBlue hover:h-auto peer-hover:h-auto">
                {renderMenu(item.children, item)}
              </ul>
            </>
          ) : (
            <DesktopLink to={parent ? `${parent.path}/${item.path}` : item.path}>{item.title}</DesktopLink>
          )
        }
      </li>


    ));
  };
  // #################################
  // OUTPUT
  // #################################
  return (
    <ul className="hidden sm:flex px-4 ont-UhhBC text-2xl container flex justify-between text-UhhWhite font-UhhSLC">
      {filteredSitemap[0]?.children && renderMenu(filteredSitemap[0].children)}
    </ul>
  );
}

export default React.memo(DesktopNav);