import React, { useEffect, useState } from 'react';
import { useAuth } from '../../../contexts/Auth/AuthState';
import DesktopNav from './DesktopNav';
import MobileNav from './MobileNav';
import { sitemap } from "/src/routes/Sitemap";


function Navbar(props) {
  // #################################
  // HOOKS
  // #################################
  // ### FILTERED SITEMAP
  const [filteredSitemap, setFilteredSitemap] = useState([]);

  // ### CONNECT AUTH CONTEXT
  const { currentUser } = useAuth();

  useEffect(() => {
    // fetch all links for navbars
    const [overall] = sitemap.filter((item) => item.title === 'MenuBar');

    // fetch all children from /
    const [home] = overall.children;

    // recursively filter sitemap
    function flatFilter(nestedProp, searchKey, searchValue, arr) {
      return arr.filter(o => {
        // slightly customized for searchKey = object
        let keep = o[searchKey] && o[searchKey].hasOwnProperty(searchValue);

        // dont show items that are above the current user role
        if (o.gateKeeper && currentUser && currentUser.role < o.gateKeeper) keep = false;


        if (keep && o[nestedProp]) {
          o[nestedProp] = flatFilter(nestedProp, searchKey, searchValue, o[nestedProp]);
        }
        return keep;
      });
    }

    setFilteredSitemap(flatFilter('children', 'handle', 'crumb', [home]));
  }, []);
  // #################################
  // FUNCTIONS
  // #################################


  // #################################
  // OUTPUT
  // #################################
  return (
    <nav className="row-start-2 col-span-full sm:flex sm:justify-center sm:bg-UhhBlue">
      {/* mobile */}
      <MobileNav filteredSitemap={filteredSitemap} showMobileNav={props.showMobileNav} toggleMobileNav={props.toggleMobileNav} />

      {/* desktop - render starts with children of home */}
      <DesktopNav filteredSitemap={filteredSitemap} />
    </nav >

  );
}

export default React.memo(Navbar);