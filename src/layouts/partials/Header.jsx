import React from 'react';
import UhhImg from '/src/assets/img/uhh_logo.svg?react';
import PortalImg from '/src/assets/img/portal_logo.svg?react';
import { Link } from 'react-router-dom';
import { RiFilter2Line } from 'react-icons/ri';
import Hamburger from 'hamburger-react';


function Header({ layout, showMobileNav, toggleMobileNav }) {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################

  // CLEAN LAYOUT
  if (layout === 'clean') {
    return (
      <header role="banner" className="px-4 py-2">
        <a className="block h-16" href="https://www.uni-hamburg.de" target="_blank" title="UHH [external]">
          <UhhImg />
        </a>
      </header>
    );
  } else {

    // MAIN LAYOUT
    return (
      <header role="banner" className='row-start-1 col-span-full sm:flex justify-center'>
        {/* mobile */}
        <div className="flex justify-between items-center bg-UhhBlue text-UhhWhite px-4 py-2 sm:hidden">
          <a className="hidden h-16" href="https://www.uni-hamburg.de" target="_blank" title="UHH [external]">
            <UhhImg />
          </a>
          <Link className="flex" to="/">
            <div className="h-8 pr-2">
              <PortalImg alt='Logo of ZBH Portal' />
            </div>
            <h2>ZBH-Portal</h2>
          </Link>
          <span className="h-12 flex items-center gap-x-4">
            <div className="h-8 w-8">
              <RiFilter2Line className='text-3xl' />
            </div>
            <div className="h-full w-8">
              <Hamburger label='show navigation' toggled={showMobileNav} onToggle={toggleMobileNav} duration={0.3} />
            </div>
          </span>
        </div>
        {/* desktop */}
        <div className="hidden text-UhhWhite px-4 py-2 sm:flex justify-between container">
          <a className="inline-block h-20" href="https://www.uni-hamburg.de" target="_blank" title="UHH [external]">
            <UhhImg />
          </a>
          <Link className="flex items-center" to="/">
            <div className="text-UhhBlue h-20 pr-2">
              <PortalImg />
            </div>
            <h2 className="font-UhhBC">
              <div className="text-UhhGrey">ZBH</div>
              <div className="text-UhhBlue">Portal</div>
            </h2>
          </Link>
        </div>
      </header>
    );
  }
}

export default React.memo(Header);