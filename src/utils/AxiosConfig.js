import axios from "axios";

// ### CREATE BASE INSTANCE
// defaults, used by all api requests
const api = axios.create({
  baseURL: `${import.meta.env.VITE_BACKEND_URL}:${import.meta.env.VITE_BACKEND_PORT}/${import.meta.env.VITE_BACKEND_PATH}`,
});


// ### REQUEST INTERCEPTOR 
// injects accessToken if exists
api.interceptors.request.use(
  (config) => {
    // fetch accessToken 
    // TODO: don't store accessToken in localStorage, keep in memory only
    const accessToken = JSON.parse(localStorage.getItem('accessToken')) || null;
    // inject if exists
    if (accessToken) {
      config.headers["Authorization"] = 'Bearer ' + accessToken;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);


// ### RESPONSE INTERCEPTOR 
// refreshes accessToken if needed
api.interceptors.response.use(
  (res) => {
    return res;
  },
  async (err) => {
    // console.log(err);
    // save original request config
    const originalConfig = err.config;
    // if access denied and not a retry already
    // BUG: Possible infinit loop because _retry isn't set at runtime
    // the loop starts if refreshToken is invalid && backend responds with 403
    // console.log(originalConfig);
    // console.log(JSON.stringify(originalConfig));
    if (originalConfig && err?.response?.status === 403 && originalConfig._retry !== true) {
      // patch config to remember it's a retry
      originalConfig._retry = true;
      console.log('trying to refresh the accessToken and rerun the request');
      // console.log('retry', err.code, originalConfig._retry);
      // refresh access token
      try {
        const result = await api.get(
          '/auth',
          { withCredentials: true }
        );
        // TODO: don't store accessToken in localStorage, keep in memory only
        localStorage.setItem("accessToken", JSON.stringify(result.data.accessToken));
        // run retry
        return api(originalConfig);

      } catch (error) {
        return Promise.reject(error);
      }
    }
    return Promise.reject(err);

  }
);

export default api;