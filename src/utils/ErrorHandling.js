import { toast } from 'react-toastify';

// ### MERGE BACKEND ERRORS INTO FRONTEND
export function mergeBackendValidation(status, errors, setError) {
  // global error
  if (errors.message) {
    toast.error(errors.message, {
      theme: "colored"
    });
    if (setError) {
      setError('root', { type: status, message: errors.message });
    }
  }

  // exit if no validation errors returned
  if (!errors.validationErrors) return;
  // set single field validation errors
  const validationErrors = errors.validationErrors;
  Object.keys(validationErrors).forEach(function (field) {
    setError(field, { message: validationErrors[field] });
  });

  return;
}

// ### DISPLAY FLASH MESSAGES (mostly success from backend)
export function setFlashMsg(message) {
  toast.success(message);
}