import { lazy, Suspense } from 'react';
import PrivateRoute from './PrivateRoute';

/**
 * Lazily load the mentioned component which resides in the page directory
 * This method will be used in routes so that the files are loaded only
 * When users are on that route
 */
export function loadComponent(componentPath, lazyLoad, privateRoute, gateKeeper = 0) {

  lazyLoad = typeof lazyLoad !== "undefined" ? lazyLoad : true;
  privateRoute = typeof privateRoute !== "undefined" ? privateRoute : false;

  // It is not possible to use a fully dynamic import statement, such as import(foo). Because foo could potentially be any path to any file in your system or project.
  // The import() must contain at least some information about where the module is located.

  const Component = lazyLoad ? lazy(() => import(/* @vite-ignore */`../pages/${componentPath}`)) : import(/* @vite-ignore */`../pages/${componentPath}`);

  let element = privateRoute ? <PrivateRoute><Component /></PrivateRoute> : <Component />;

  element = lazyLoad ? <Suspense fallback={<span>Loading...</span>}>{element}</Suspense> : element;

  // Wrapping around the suspense component is mandatory
  return element;
}


