import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import { useAuth } from '../contexts/Auth/AuthState';

function PrivateRoute({ children }) {
  // #################################
  // HOOKS
  // #################################
  // ### MAKE USE OF location
  const location = useLocation();

  // #################################
  // FUNCTIONS
  // #################################
  // ### IMPORT CurrentUser FUNCTION FROM AuthContext
  const { currentUser } = useAuth();

  // #################################
  // OUTPUT
  // #################################
  // if user is logged in, load children
  // otherwise load login page, but remember originally requested page in location state.redirectTo
  return currentUser ? children : <Navigate to='/login' replace state={{ redirectTo: location }} />;
}

export default PrivateRoute;