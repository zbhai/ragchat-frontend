import { Link } from 'react-router-dom';
import CleanLayout from '../layouts/CleanLayout';
import MainLayout from "../layouts/MainLayout";
import { loadComponent } from "./WrapRoutes";


export const sitemap = [{
  title: 'MenuBar', element: <MainLayout />,
  children: [{
    // HOME
    title: 'Home',
    path: '/',
    handle: { crumb: () => <Link to="/">ZBH Portal</Link> },
    children: [
      {
        index: true,
        element: loadComponent('Home', true, true)
      },
      // ONBOARDING
      {
        title: 'Onboarding',
        path: '/onboarding',
        handle: { crumb: () => <Link to="/onboarding">Onboarding</Link> },
        children: [
          { index: true, element: loadComponent('Onboarding/Onboarding') },
          { title: 'Chat', path: ':id', element: loadComponent('Onboarding/Onboarding') }

        ]
      },
      // ADMIN
      {
        title: 'Config',
        path: '/config',
        handle: { crumb: () => <Link to="/config">Config</Link> },
        children: [
          { index: true, element: loadComponent('Config/Config') },
          { title: 'Chat', path: ':id', element: loadComponent('Config/Onboarding') }

        ]
      },
      // USER
      {
        title: 'Users', gateKeeper: 4, path: '/users', element: loadComponent('User/List/Users', true, true), handle: { crumb: () => <Link to="/users">Users</Link> }
      },
      // PROFILE
      {
        title: 'Profile', path: '/profile', handle: { crumb: () => <Link to="/profile">Profile</Link> },
        children: [
          { index: true, element: loadComponent('User/Profile', true, true) },
          { title: 'Logout', path: 'logout', element: loadComponent('User/Logout', true, true), handle: { crumb: () => <Link to="/profile/logout">Logout</Link> } }
        ]
      },
      // // LOGOUT
      // {
      //   title: 'Logout',
      //   path: '/logout',
      //   element: loadComponent('User/Logout', true, true),
      //   handle: { crumb: () => <Link to="/profile/logout">Logout</Link> }
      // }
    ]
  }]
}, {
  title: 'Others', element: <CleanLayout />, children: [
    // SIGNUP
    {
      path: '/signup', children: [
        { index: true, element: loadComponent('User/Signup') },
        { path: 'resend_token', element: loadComponent('User/ResendVerificationToken') },
        { path: ':token', element: loadComponent('User/Verify') }
      ]
    },
    // LOGIN
    { path: '/login', element: loadComponent('User/Login') },
    // FORGOT PASSWORD
    { path: '/reset_password', element: loadComponent('User/ForgotPassword') },
    // RESET PASSWORD
    { path: '/reset_password/:token', element: loadComponent('User/ResetPassword') },
    // ERROR
    { path: '*', element: loadComponent('Err404') }
  ]
}];