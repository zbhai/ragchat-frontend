import { USER_ACTIONS } from "./AuthTypes";
const authReducer = (state, action) => {
  switch (action.type) {
    case USER_ACTIONS.SET:
      localStorage.setItem("user", JSON.stringify(action.payload));
      return action.payload;
    case USER_ACTIONS.DROP:
      localStorage.removeItem("user");
      localStorage.removeItem("accessToken");
      return null;
    default:
      return state;
  }
};
export default authReducer;