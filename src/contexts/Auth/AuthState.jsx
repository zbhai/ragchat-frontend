import React, { useContext, useReducer, useState } from 'react';
import api from '../../utils/AxiosConfig';
import AuthContext from './AuthContext';
import authReducer from './AuthReducer';
import { USER_ACTIONS } from './AuthTypes';

// ### EXPORT useContext TO REDUCE NEEDED CODE IN CLIENT FILES
export function useAuth() {
  return useContext(AuthContext);
}

function AuthState({ children }) {
  // #################################
  // HOOKS
  // #################################
  // ### CURRENTUSER_ACTIONS
  // set default user for first page load and hard reloads (<CTRL +> F5)
  const initial_currenUser = JSON.parse(localStorage.getItem('user')) || null;
  const [currentUser, dispatchCurrentUser] = useReducer(authReducer, initial_currenUser);
  // ### ACCESSTOKEN
  const [accessToken, setAccessToken] = useState();

  // #################################
  // FUNCTIONS
  // #################################

  // ### LOGIN
  async function login(credentials) {
    const result = await api.post(
      '/auth/login',
      credentials,
      { withCredentials: true }
    );
    // set current user to login and merge accessToken into currentUser
    dispatchCurrentUser({ type: USER_ACTIONS.SET, payload: { ...result.data.document } });
    setAccessToken(result.data.accessToken);
    // TODO: don't store accessToken in localStorage, keep in memory only
    localStorage.setItem("accessToken", JSON.stringify(result.data.accessToken));
    return result;
  }

  // ### HANDLE LOGOUT
  async function logout() {
    dispatchCurrentUser({ type: USER_ACTIONS.DROP });
    const result = await api.delete(
      '/auth',
      { withCredentials: true }
    );
    return result;
  }

  // ### REQUEST RESEND VERIFICATION TOKEN
  function requestVerificationToken(email) {
    return api.post('/auth/verification', { email });
  }

  // ### REQUEST PASSWORD RESET
  function requestPasswordReset(email) {
    return api.post('/auth/password-reset', { email });
  }

  // ### UPDATE USER_ACTIONS
  async function update(id, user) {
    // remove password keys if not set to avoid emtying password field
    if (user && !user.password) delete user.password;
    if (user && !user.confirmPassword) delete user.confirmPassword;
    // send data to backend
    return api.patch(`/users/${id}`,
      user
    );
  }

  // ### RETURN
  return (
    <AuthContext.Provider
      value={{
        login,
        currentUser,
        update,
        logout,
        requestVerificationToken,
        requestPasswordReset,
        USER_ACTIONS,
        dispatchCurrentUser
      }}>
      {children}
    </AuthContext.Provider>
  );
};

export default React.memo(AuthState); 