export const CHAT_ACTIONS = {
  SET_CHATS: 'set_chats',
  SET_HEADINGS: 'set_headings',
  SET_HISTORY: 'set_history',
  UPDATE_CHATID: 'update_chatid',
  SET_MODELS: 'set_models'
};