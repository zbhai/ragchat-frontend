import { CHAT_ACTIONS } from './ChatTypes';

const chatReducer = (state, action) => {
  switch (action.type) {
    case CHAT_ACTIONS.SET_CHATS:
    case CHAT_ACTIONS.SET_HEADINGS:
    case CHAT_ACTIONS.UPDATE_CHATID:
      { return action.payload; }


    case CHAT_ACTIONS.SET_HISTORY: {
      if (!action.payload.id) return [];
      // define a function to find the chat
      const isSelectedChatId = element => element.id === action.payload.id;
      // get index of matching item
      const index = action.payload.chats.findIndex(isSelectedChatId);
      // return history of chat
      return action.payload.chats[index].chatHistory;
    }

    case CHAT_ACTIONS.SET_MODELS: {
      const models = action.payload.models;
      const modelNames = models.map(model => {
        return { title: model.name, _id: model.name };
      });
      return modelNames;
    }

    default:
      { return state; }
  }
};

export default chatReducer;