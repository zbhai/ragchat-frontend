import React from 'react';
import { Helmet } from 'react-helmet-async';
import { useNavigate } from "react-router-dom";

function Err404() {
  // #################################
  // HOOKS
  // #################################
  const redirect = useNavigate();
  // #################################
  // FUNCTIONS
  // #################################
  function handleClick(event) {
    event.preventDefault();
    redirect(-1);
  }

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {/* render page title */}
      <Helmet><title>[{import.meta.env.VITE_APP_NAME}] Error</title></Helmet>
      <h1 className="text-8xl text-center text-UhhGrey">404</h1>
      <h2>Page not found</h2>
      <p className="mt-8">We're sorry for the inconvenience, but we can lead you</p>
      <span className="inline-block w-full h-16 px-8 mb-4 border-box leading-[4rem] text-center align-middle bg-UhhBlue text-UhhWhite font-UhhBC cursor-pointer" onClick={handleClick}>back</span>
    </>
  );
}

export default React.memo(Err404);
