import React from 'react';
import Heading from '../components/font/Heading';

function Home() {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <Heading level="1">Home</Heading>
  );
}

export default React.memo(Home);