import React from 'react';
import { Helmet } from 'react-helmet-async';
import Chats from '../../components/chat/Chats';
import Messages from '../../components/chat/Messages';
import PromptInput from '../../components/chat/PromptInput';
import Heading from '../../components/font/Heading';
import ChatState from '../../contexts/Chat/ChatState';


function Onboarding() {
  // #################################
  // HOOKS
  // #################################


  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <div className="h-full grid grid-rows-[auto_1fr_auto] grid-cols-[auto_1fr]">
      {/* render page title */}
      <Helmet><title>[{import.meta.env.VITE_APP_NAME}] Onboarding</title></Helmet>

      <Heading level="1" className="col-span-2">Onboarding</Heading>
      <ChatState>
        <Chats />
        <Messages />
        <PromptInput />
      </ChatState>
    </div>
  );
}

export default React.memo(Onboarding);;