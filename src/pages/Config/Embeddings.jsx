import React, { useEffect, useState } from 'react';
import api from '../../utils/AxiosConfig';
import { mergeBackendValidation } from '../../utils/ErrorHandling';
import Delete from './Embeddings/Delete';
import Status from './Embeddings/Status';
import Update from './Embeddings/Update';

function Embeddings() {
  // #################################
  // HOOKS
  // #################################
  // ### SET DATA
  const [status, setStatus] = useState({});

  // ### FETCH MODELS
  useEffect(() => {
    // ### on run exec this code
    const controller = new AbortController();
    const getStatus = async () => {
      try {
        // fetch all items and store them in state
        const result = await api.get('/embeddings', {
          signal: controller.signal
        });
        setStatus(result.data);
      } catch (error) {
        mergeBackendValidation(error.response.status, error.response.data);
      }
    };
    getStatus();
    // ### return will be executed on unmounting this component
    return () => {
      // on unmount abort request
      controller.abort();
    };
    // ### opt. 2. argument: when to run this hook
  }, []);

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <section>
      {/* <!-- header --> */}
      <div className="group sticky top-0 bg-UhhWhite z-10 mb-4 font-UhhBC text-2xl">
        {/* <!-- number --> */}
        <span className="text-UhhRed">02 </span>
        {/* <!-- title --> */}
        Embeddings
        {/* <!-- line --> */}
        <div className="absolute w-[50vw] right-[50%] h-1 bg-UhhRed"></div>
      </div>

      <fieldset>
        {/* rag status */}
        <Status status={status} />
        {/* update embeddings */}
        <Update setStatus={setStatus} />
        {/* delete embeddings */}
        <Delete setStatus={setStatus} />
      </fieldset>
    </section>
  );
}

export default React.memo(Embeddings);