import React from 'react';
import JsonToHtmlDL from '../../../components/boxes/JsonToHtmlDL';
import Heading from '../../../components/font/Heading';

function Status({ status }) {
  // #################################
  // HOOKS
  // #################################

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <div className='self-start bg-white border border-UhhLightGrey rounded-lg shadow-lg p-3'>
      <Heading level="4">Status</Heading>
      <JsonToHtmlDL jsonContent={status} />
    </div>
  );
}

export default React.memo(Status);