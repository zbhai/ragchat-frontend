import React, { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import ConfirmBox from '../../../components/boxes/ConfirmBox';
import Heading from '../../../components/font/Heading';
import Submit from '../../../components/form/Submit';
import { useAuth } from '../../../contexts/Auth/AuthState';
import api from '../../../utils/AxiosConfig';
import { mergeBackendValidation, setFlashMsg } from '../../../utils/ErrorHandling';

function Delete({ setStatus }) {
  // #################################
  // HOOKS
  // #################################
  // ### CONNECT AUTH CONTEXT
  const { currentUser } = useAuth();
  // ### FORM HOOKS
  const methods = useForm();
  // ### CONFIRM DIALOG
  const [confirmDialog, setConfirmDialog] = useState({ open: false, item: {} });

  // #################################
  // FUNCTIONS
  // #################################
  // security
  const handleConfirm = () => {
    setConfirmDialog({ open: true, displayName: 'Vector Database' });
  };

  // ### HANDLE INSTALL ITEM
  const handleDelete = async () => {
    try {
      // install item in backend
      const result = await api.delete('/embeddings');
      // renew status
      setStatus(result.data);
      // show message from backend
      setFlashMsg(result?.data?.message);
    } catch (error) {
      mergeBackendValidation(error.response.status, error.response.data);
    }
  };

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {(currentUser?.role >= 2) ?
        <div className='self-start bg-white border border-UhhLightGrey rounded-lg shadow-lg p-3'>
          <FormProvider {...methods} >
            <Heading level="4">Delete Embedding Collection</Heading>
            <form onSubmit={methods.handleSubmit(handleConfirm)} className='md:w-1/3'>
              <Submit variant='destructive' size='sm' value={methods.formState.isSubmitting ? 'deleting...' : 'delete'} />
            </form>
          </FormProvider>


          <ConfirmBox confirmDialog={confirmDialog} closeDialog={() => setConfirmDialog({ ...confirmDialog, open: false })} handleProceed={() => { handleDelete(confirmDialog.idToDelete); }} />

        </div>

        : null}
    </>
  );
}

export default React.memo(Delete);