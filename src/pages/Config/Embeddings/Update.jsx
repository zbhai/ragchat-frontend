import React, { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import JsonToHtmlDL from '../../../components/boxes/JsonToHtmlDL';
import Tooltip from '../../../components/boxes/Tooltip';
import Heading from '../../../components/font/Heading';
import Submit from '../../../components/form/Submit';
import { useAuth } from '../../../contexts/Auth/AuthState';
import api from '../../../utils/AxiosConfig';
import { mergeBackendValidation, setFlashMsg } from '../../../utils/ErrorHandling';

function Update({ setStatus }) {
  // #################################
  // HOOKS
  // #################################
  // ### CONNECT AUTH CONTEXT
  const { currentUser } = useAuth();

  // ### SET DATA
  const [data, setData] = useState({});

  // ### FORM HOOKS
  const methods = useForm();
  // #################################
  // FUNCTIONS
  // #################################
  // ### UPDATE EMBEDDINGS
  const handleUpdate = async () => {
    try {
      // update
      const update = await api.patch('/embeddings');
      // renew status
      setData(update.data);

      // renew status
      const status = await api.get('/embeddings');
      setStatus(status.data);


      // show message from backend
      setFlashMsg(update.data?.message);
    } catch (error) {
      mergeBackendValidation(error.response.status, error.response.data);
    }
  };
  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {(currentUser?.role >= 2) ?
        <div className='self-start bg-white border border-UhhLightGrey rounded-lg shadow-lg p-3'>
          <FormProvider {...methods}>
            <Heading level="4">Update Embeddings
              <Tooltip><p className='text-base'>based on local RAG Files</p></Tooltip>
            </Heading>
            <form onSubmit={methods.handleSubmit(handleUpdate)} className='md:w-1/3'>
              <Submit size='sm' value={methods.formState.isSubmitting ? 'updating...' : 'update'} />
            </form>
            <details className='py-4 border-b border-grey-lighter'>
              <summary>Update Result</summary>
              <JsonToHtmlDL jsonContent={data} />
            </details>
          </FormProvider>
        </div>
        : null}
    </>
  );
}

export default React.memo(Update);