import React, { useEffect, useState } from 'react';
import api from '../../utils/AxiosConfig';
import { mergeBackendValidation } from '../../utils/ErrorHandling';
import Models from './AI/Models';
import NewModel from './AI/NewModel';
import AIStatus from './AI/Status';

function AIModels() {
  // #################################
  // HOOKS
  // #################################
  // ### SET TABLE DATA
  const [data, setData] = useState({});

  // ### FETCH MODELS
  useEffect(() => {
    // ### on run exec this code
    const controller = new AbortController();
    const getDocument = async () => {
      try {
        // fetch all items and store them in state
        const items = await api.post('/ai/models', { filter: '' }, {
          signal: controller.signal
        });

        setData(items.data.models);
      } catch (error) {
        mergeBackendValidation(error.response.status, error.response.data);
      }
    };
    getDocument();
    // ### return will be executed on unmounting this component
    return () => {
      // on unmount abort request
      controller.abort();
    };
    // ### opt. 2. argument: when to run this hook
  }, []);



  // #################################
  // FUNCTIONS
  // #################################



  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {/* AI */}
      <section>
        {/* <!-- header --> */}
        <div className="group sticky top-0 bg-UhhWhite z-10 mb-4 font-UhhBC text-2xl">
          {/* <!-- number --> */}
          <span className="text-UhhRed">01 </span>
          {/* <!-- title --> */}
          AI
          {/* <!-- line --> */}
          <div className="absolute w-[50vw] right-[50%] h-1 bg-UhhRed"></div>
        </div>

        <fieldset>
          {/* ai status */}
          <AIStatus />
          {/* new model */}
          <NewModel data={data} setData={setData} />
          {/* model list */}
          <Models data={data} setData={setData} />

        </fieldset>



      </section>
    </>
  );
}

export default React.memo(AIModels);