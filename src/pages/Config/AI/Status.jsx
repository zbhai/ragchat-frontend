import React, { useEffect, useState } from 'react';
import { RiWifiFill, RiWifiOffFill } from "react-icons/ri";
import Heading from '../../../components/font/Heading';
import api from '../../../utils/AxiosConfig';
import { mergeBackendValidation } from '../../../utils/ErrorHandling';

function AIStatus() {
  // #################################
  // HOOKS
  // #################################
  // ### SET STATUS
  const [status, setStatus] = useState({});

  // ### FETCH STATUS
  useEffect(() => {
    // ### on run exec this code
    const controller = new AbortController();
    const getStatus = async () => {
      try {
        // fetch all items and store them in state
        const result = await api.get('/ai/status', {
          signal: controller.signal
        });

        setStatus(result.data.running);
      } catch (error) {
        mergeBackendValidation(error.response.status, error.response.data);
      }
    };
    getStatus();
    // ### return will be executed on unmounting this component
    return () => {
      // on unmount abort request
      controller.abort();
    };
    // ### opt. 2. argument: when to run this hook
  }, []);
  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <div className='self-start bg-white border border-UhhLightGrey rounded-lg shadow-lg p-3'>
      <Heading level="4">status <br />
        {status ?
          <RiWifiFill className='ml-4 text-UhhBlue' title='AI backend reachable' />
          : <RiWifiOffFill className='ml-4 text-UhhRed' title='AI backend offline' />}
      </Heading>
    </div>
  );
}

export default React.memo(AIStatus);