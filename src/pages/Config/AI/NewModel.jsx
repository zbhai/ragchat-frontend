import { zodResolver } from '@hookform/resolvers/zod';
import React from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { z } from 'zod';
import Heading from '/src/components/font/Heading';
import Input from '/src/components/form/Input';
import Submit from '/src/components/form/Submit';
import { useAuth } from '/src/contexts/Auth/AuthState';
import api from '/src/utils/AxiosConfig';
import { mergeBackendValidation, setFlashMsg } from '/src/utils/ErrorHandling';

function NewModel({ data, setData }) {
  // #################################
  // HOOKS
  // #################################
  // ### CONNECT AUTH CONTEXT
  const { currentUser } = useAuth();

  // ### SCHEMA
  const schema = z.object({
    model: z.string().min(1),
  });

  // ### FORM HOOKS
  const methods = useForm({
    resolver: zodResolver(schema),
    mode: 'onSubmit'
  });

  // #################################
  // FUNCTIONS
  // #################################
  // ### HANDLE INSTALL ITEM
  const handleInstall = async (inputs) => {
    try {
      // install item in backend
      const result = await api.put('/ai/models', { model: inputs.model });
      // add new model to the list
      setData([...data, result.data.model[0]]);
      // show message from backend
      setFlashMsg(result?.data?.message);
    } catch (error) {
      mergeBackendValidation(error.response.status, error.response.data);
    }
  };

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {(currentUser?.role >= 2) ?
        <div className='self-start bg-white border border-UhhLightGrey rounded-lg shadow-lg p-3'>
          <FormProvider {...methods} >
            <Heading level="4">install new model</Heading>
            <form onSubmit={methods.handleSubmit(handleInstall)} className=''>
              <Input name='model' type='text' title='Model Name' className='h-16' required={true} tooltip={<Link to='https://ollama.com/library' target='_blank' rel='noopener noreferrer'>Ollama Library</Link>} />
              <Submit size='sm' value={methods.formState.isSubmitting ? 'installing...' : 'install model'} />
            </form>
          </FormProvider>

        </div>
        : null}
    </>
  );
}

export default React.memo(NewModel);