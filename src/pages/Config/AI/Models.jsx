import React, { useState } from 'react';
import { RiDeleteBinLine, RiFileInfoLine, RiMoreLine, RiRefreshLine } from 'react-icons/ri';
import ConfirmBox from '/src/components/boxes/ConfirmBox';
import InfoBox from '/src/components/boxes/InfoBox';
import CustomTable from '/src/components/table/customTable';
import { Button } from '/src/components/ui/button';
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "/src/components/ui/dropdown-menu";
import { useAuth } from '/src/contexts/Auth/AuthState';
import api from '/src/utils/AxiosConfig';
import { mergeBackendValidation, setFlashMsg } from '/src/utils/ErrorHandling';


function Models({ data, setData }) {
  // #################################
  // HOOKS
  // #################################
  // ### CONNECT AUTH CONTEXT
  const { currentUser } = useAuth();
  // ### CONFIRM DIALOG
  const [confirmDialog, setConfirmDialog] = useState({ open: false, item: {} });
  // ### INFO DIALOG
  const [infoDialog, setInfoDialog] = useState({ open: false, title: '', description: '', body: '', item: {} });

  // ### HANDLE DELETE ITEM
  const handleDelete = async (model) => {
    try {
      // remove item from backend
      const result = await api.delete('/ai/models', { data: { model } });
      // remove item from current display
      const filteredData = data.filter(item => item.name !== model);
      setData(filteredData);
      // show message from backend
      setFlashMsg(result?.data?.message);
    } catch (error) {
      mergeBackendValidation(error.response.status, error.response.data);
    }
  };

  // ### HANDLE UPDATE ITEM
  const handleUpdate = async (model) => {
    try {
      // install item in backend
      const result = await api.put('/ai/models', { model });
      // show message from backend
      setFlashMsg(result?.data?.message);
    } catch (error) {
      mergeBackendValidation(error.response.status, error.response.data);
    }
  };

  // ### HANDLE ITEM DETAILS
  const handleDetails = async (model) => {
    try {
      // install item in backend
      const result = await api.post('/ai/model', { model });
      // show message from backend
      setInfoDialog({ open: true, title: model, description: 'model details', body: result?.data });
    } catch (error) {
      mergeBackendValidation(error.response.status, error.response.data);
    }
  };

  // #################################
  // FUNCTIONS
  // #################################
  // ### DEFINE TABLE ROWS
  const columns = [
    {
      accessorKey: 'name',
      header: 'name',
      enableColumnFilter: true,
      enableHiding: false,
      filterFn: 'regex',
      // cell: (props) => <span>{props.getValue()}</span>
      cell: (props) => {
        return (props.row.original.name);
      }

    }, {
      accessorKey: 'size',
      header: 'size in bytes',
      enableColumnFilter: true,
      enableHiding: true,
      filterFn: 'includesString',
      cell: (props) => <span>{Number(props.getValue())}</span>
      // cell: (props) => {
      //   return (Number(props.row.original.size) / 1024 / 1024 / 1024).toFixed(2).toString();
      // }
    }, {
      id: 'details.parameter_size',
      accessorKey: 'details.parameter_size',
      header: 'parameters',
      enableColumnFilter: true,
      enableHiding: true,
      filterFn: 'regex',
      cell: (props) => <span>{props.getValue()}</span>
      // cell: (props) => {
      //   return (props.row.original.details.parameter_size);
      // }

    }, {
      id: "actions",
      enableHiding: false,
      meta: {
        cellClassName: 'text-center w-16',
      },
      cell: ({ row }) => {
        const item = row.original;
        return (
          <DropdownMenu>
            <DropdownMenuTrigger asChild>
              <Button title="open menu">
                <span className="sr-only">Open menu</span>
                <RiMoreLine />
              </Button>
            </DropdownMenuTrigger>
            <DropdownMenuContent align="end">
              <DropdownMenuLabel>Actions</DropdownMenuLabel>
              <DropdownMenuItem className='flex items-center space-x-2 w-full' onClick={() => handleDetails(item.name)}>
                <span><RiFileInfoLine /></span>
                <span>show details</span>
              </DropdownMenuItem>
              {currentUser.role > 2 ?
                <>
                  <DropdownMenuItem className='cursor-pointer flex items-center space-x-2 w-full' onClick={() => handleUpdate(item.name)}>
                    <span><RiRefreshLine /></span>
                    <span>update model</span>
                  </DropdownMenuItem>
                  <DropdownMenuSeparator />
                  <DropdownMenuItem className='cursor-pointer text-UhhRed hover:text-UhhWhite hover:bg-UhhRed' onClick={() => setConfirmDialog({ open: true, idToDelete: item.name, displayName: item.name })}>
                    <span className='flex items-center space-x-2 w-full'>
                      <span><RiDeleteBinLine /></span>
                      <span>delete model</span>
                    </span>
                  </DropdownMenuItem>
                </>
                : null}
            </DropdownMenuContent>
          </DropdownMenu>
        );
      },
    }
  ];
  // #################################
  // OUTPUT
  // #################################
  return (
    <div className='self-start bg-white border border-UhhLightGrey rounded-lg shadow-lg p-3'>
      {/* table */}
      <CustomTable columns={columns} data={data} title='installed models' />

      {/* confirmDialog */}
      <ConfirmBox confirmDialog={confirmDialog} closeDialog={() => setConfirmDialog({ ...confirmDialog, open: false })} handleProceed={() => { handleDelete(confirmDialog.idToDelete); }} />

      {/* infoDialog */}
      <InfoBox infoDialog={infoDialog} closeDialog={() => setInfoDialog({ ...infoDialog, open: false })} />

    </div>
  );
}

export default React.memo(Models);