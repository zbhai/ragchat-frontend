import React from 'react';
import { Helmet } from 'react-helmet-async';
import { useAuth } from '../../contexts/Auth/AuthState';
import AIModels from './AIModels';
import Embeddings from './Embeddings';


function Config() {
  // #################################
  // HOOKS
  // #################################
  // ### CONNECT AUTH CONTEXT
  const { currentUser } = useAuth();

  // #################################
  // FUNCTIONS
  // #################################

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {/* render page title */}
      <Helmet><title>[{import.meta.env.VITE_APP_NAME}] Config</title></Helmet>
      {/* AI */}
      <AIModels />
      {/* RAG */}
      <Embeddings />
    </>
  );
}

export default React.memo(Config);