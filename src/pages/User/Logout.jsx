import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../contexts/Auth/AuthState';
import { setFlashMsg } from '../../utils/ErrorHandling';

function Logout() {
  // #################################
  // HOOKS
  // #################################
  // ### CONNECT AUTH CONTEXT
  const { logout } = useAuth();

  // ### LOGOUT IMMEDIATLY AFTER LOADING
  useEffect(() => {
    handleLogout();
  }, []);
  // ### ENABLE Redirections
  const redirect = useNavigate();

  // #################################
  // FUNCTIONS
  // #################################

  async function handleLogout() {
    try {
      const result = await logout();
      redirect('/login');
      // set flash msg
      setFlashMsg(result.data?.msg);
    } catch (err) {
      mergeBackendValidation(500, err);
    }
  }

  // #################################
  // OUTPUT
  // #################################
  return (
    <div>You're about to log out</div>
  );
}

export default React.memo(Logout);