import { zodResolver } from '@hookform/resolvers/zod';
import React from 'react';
import { Helmet } from 'react-helmet-async';
import { FormProvider, useForm } from 'react-hook-form';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { z } from 'zod';
import Heading from '../../components/font/Heading';
import Input from '../../components/form/Input';
import Submit from '../../components/form/Submit';
import api from '../../utils/AxiosConfig';
import { mergeBackendValidation, setFlashMsg } from '../../utils/ErrorHandling';

function Verify() {
  // #################################
  // VALIDATION SCHEMA
  // #################################
  // TODO limit file size via .env
  // TODO check for file types
  const schema = z.object({
    token: z.string().min(1),
  });

  // #################################
  // HOOKS
  // #################################
  // FETCH TOKEN FROM URL
  const { token } = useParams();

  // SET FORM
  const methods = useForm({
    resolver: zodResolver(schema),
    mode: 'onSubmit',
    defaultValues: {
      token: token,
    }
  });

  // ### ENABLE REDIRECTIONS
  const redirect = useNavigate();

  // #################################
  // FUNCTIONS
  // #################################

  // ### HANDLE SUBMITTING FORM
  async function handleSendForm(inputs) {

    // TRY UPDATE
    try {
      // send data
      const result = await api.patch(`/auth/verification`, inputs);
      redirect('/login');
      // set flash message
      setFlashMsg(result.data?.message);
    } catch (error) {
      // catch the error
      mergeBackendValidation(error.response.status, error.response.data, methods.setError);
    }
  }

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {/* render page title */}
      <Helmet><title>[{import.meta.env.VITE_APP_NAME}] change E-Mail</title></Helmet>

      <Heading level="1">Verify Account</Heading>
      <FormProvider {...methods} >
        <form onSubmit={methods.handleSubmit(handleSendForm)}>
          <Input name='token' type='text' title='confirm token' required={true} />
          <Submit value='Verify' />
        </form>
      </FormProvider>

      <div className="my-4 flex justify-between">
        <Link to="/login">Back to Login</Link>
        <Link to="/signup/resend_token">Resend Token</Link>
      </div>
    </>
  );
}

export default React.memo(Verify);