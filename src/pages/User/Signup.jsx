import { zodResolver } from '@hookform/resolvers/zod';
import React from 'react';
import { Helmet } from 'react-helmet-async';
import { FormProvider, useForm } from 'react-hook-form';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import validator from 'validator';
import { z } from "zod";
import Heading from '../../components/font/Heading';
import Input from '../../components/form/Input';
import Submit from '../../components/form/Submit';
import { useAuth } from '../../contexts/Auth/AuthState';
import api from '../../utils/AxiosConfig';
import { mergeBackendValidation, setFlashMsg } from '../../utils/ErrorHandling';

function Signup() {

  // #################################
  // VALIDATION SCHEMA
  // #################################
  const schema = z.object({
    name: z.string().min(1),
    username: z.string().min(1),
    email: z.string().email(),
    password: z.string().refine((val) => val && validator.isStrongPassword(val), {
      message: 'This value must be min 6 characters long and contain uppercase, lowercase, number, specialchar.',
    }),
    confirmPassword: z.string(),
  }).refine((data) => data.password === data.confirmPassword, {
    message: "Passwords don't match",
    path: ["confirmPassword"],
  });
  // #################################
  // HOOKS
  // #################################
  // ### CONNECT AUTH CONTEXT
  const { signup } = useAuth();
  // ### MAKE USE OF NAVIGATION
  const redirect = useNavigate();

  // ### MAKE USE OF location state to fetch former requested page
  const { state } = useLocation();
  // ### PREPARE FORM
  const methods = useForm({
    resolver: zodResolver(schema),
    mode: 'onSubmit',
    defaultValues: {
      name: '',
      username: '',
      email: '',
      password: '',
      confirmPassword: ''
    }
  });


  // #################################
  // FUNCTIONS
  // #################################
  // ### HANDLE SUBMITTING LOGIN FORM
  async function handleSendForm(record) {
    try {
      // send data to API
      const result = await api.post(`/users`, record);
      // if successfull redirect to login page
      redirect(`/signup/insertToken`);
      // FIX: flash message not dislayed
      setFlashMsg(result.data?.message);

    } catch (err) {
      // merge front & backend validation errors
      mergeBackendValidation(err.response.status, err.response.data, methods.setError);
    }
  }

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {/* render page title */}
      <Helmet><title>[{import.meta.env.VITE_APP_NAME}]</title></Helmet>

      <Heading level="1">ZBH-Portal LogIn</Heading>
      <FormProvider {...methods} >
        <form onSubmit={methods.handleSubmit(handleSendForm)}>
          <Input name='name' type='text' title='Name' className='h-16' autoFocus={true} />
          <Input name='username' type='text' title='username' className='h-16' />
          <Input name='email' type='mail' title='E-Mail' className='h-16' />
          <Input name='password' type='password' title='password' className='h-16' />
          <Input name='confirmPassword' type='password' title='confirm password' className='h-16' />
          <Submit value='Signup' />
        </form>
      </FormProvider>

      <div className="mt-4 flex justify-between">
        <Link to="/login">Back to Login</Link>
        <Link to="/signup/insertToken">verify Account</Link>
      </div>
    </>
  );
}

export default Signup;