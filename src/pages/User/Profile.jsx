import { zodResolver } from '@hookform/resolvers/zod';
import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { FormProvider, useForm } from 'react-hook-form';
import { isStrongPassword } from 'validator';
import { z } from "zod";
import Input from '../..//components/form/Input';
import Heading from '../../components/font/Heading';
import Submit from '../../components/form/Submit';
import { useAuth } from '../../contexts/Auth/AuthState';
import { mergeBackendValidation, setFlashMsg } from '../../utils/ErrorHandling';

function Profile() {
  // #################################
  // VALIDATION SCHEMA
  // #################################
  // TODO limit file size via .env
  // TODO check for file types
  const schema = z.object({
    name: z.string().min(1),
    username: z.string().min(1),
    email: z.string().email(),
    password: z.string().refine((val) => val && isStrongPassword(val), {
      message: 'This field must be min 6 characters long and contain uppercase, lowercase, number, specialchar.',
    }).nullish().or(z.literal('')),
    confirmPassword: z.string().nullish().or(z.literal('')),
  }).refine((data) => data.password === data.confirmPassword, {
    message: "Passwords don't match",
    path: ["confirmPassword"],
  });

  // #################################
  // HOOKS
  // #################################
  // ### CONNECT AUTH CONTEXT
  const { currentUser, dispatchCurrentUser, USER_ACTIONS, update } = useAuth();
  // ### PREPARE FORM
  const methods = useForm({
    resolver: zodResolver(schema),
    mode: 'onSubmit',
    defaultValues: currentUser
  });

  // ### RESET FORM AFTER SUCCESSFUL SUBMIT
  useEffect(() => {
    if (!methods.formState.isSubmitSuccessful) return;
    methods.reset(currentUser);
    methods.setValue('confirmPassword', '');
    methods.setValue('password', '');
  }, [methods.formState]);

  // #################################
  // FUNCTIONS
  // #################################
  // ### HANDLE SUBMITTING FORM
  async function handleSendForm(inputs) {
    // TRY UPDATE
    try {
      // send data to update function
      const result = await update(
        currentUser._id,
        inputs
      );
      // update currentUser
      await dispatchCurrentUser({ type: USER_ACTIONS.SET, payload: result.data.document });
      setFlashMsg(result.data?.message);
    } catch (error) {
      // catch the error
      mergeBackendValidation(error.response.status, error.response.data, methods.setError);
    }
  }

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {/* render page title */}
      <Helmet><title>[{import.meta.env.VITE_APP_NAME}] Profile</title></Helmet>

      <Heading level="1" className="col-span-2">Profile</Heading>
      <FormProvider {...methods} >
        <form onSubmit={methods.handleSubmit(handleSendForm)}>
          <fieldset>
            <Input name='name' type='text' title='Name' required={true} autoFocus={true} />
            <Input name='username' type='text' title='Username' required={true} />
            <Input name='email' type='email' title='eMail' required={true} />
          </fieldset>

          <fieldset>
            <Input name='password' type='password' title='password' placeholder='Leave blank to keep the same' />
            <Input name='confirmPassword' type='password' title='confirm password' placeholder='Leave blank to keep the same' />
          </fieldset>

          <Submit value='update' />
        </form>
      </FormProvider>
    </>
  );
}

export default React.memo(Profile);;