import { zodResolver } from '@hookform/resolvers/zod';
import React from 'react';
import { Helmet } from 'react-helmet-async';
import { FormProvider, useForm } from 'react-hook-form';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { isStrongPassword } from 'validator';
import { z } from 'zod';
import Heading from '../../components/font/Heading';
import Input from '../../components/form/Input';
import Submit from '../../components/form/Submit';
import { useAuth } from '../../contexts/Auth/AuthState';
import api from '../../utils/AxiosConfig';
import { mergeBackendValidation, setFlashMsg } from '../../utils/ErrorHandling';


function ResetPasswordForm() {
  // #################################
  // VALIDATION SCHEMA
  // #################################
  const schema = z.object({
    token: z.string().min(1),
    password: z.string().refine((val) => val && isStrongPassword(val), {
      message: 'This field must be min 6 characters long and contain uppercase, lowercase, number, specialchar.',
    }),
    confirmPassword: z.string(),
  }).refine((data) => data.password === data.confirmPassword, {
    message: "Passwords don't match",
    path: ["confirmPassword"],
  });

  // #################################
  // HOOKS
  // #################################
  // ### CONNECT AUTH CONTEXT
  const { logout } = useAuth();
  // FETCH TOKEN FROM URL
  const { token } = useParams();

  // ### PREPARE FORM
  const methods = useForm({
    resolver: zodResolver(schema),
    mode: 'onSubmit',
    defaultValues: {
      token: token,
    }
  });

  // ### ENABLE REDIRECTIONS
  const redirect = useNavigate();

  // #################################
  // FUNCTIONS
  // #################################


  // ### HANDLE SUBMITTING FORM
  async function handleSendForm(inputs) {
    // TRY UPDATE
    try {
      // send data
      const result = await api.patch(`/auth/password-reset`, inputs);
      await logout();
      redirect('/login');
      // set flash message
      setFlashMsg(result.data?.message);
    } catch (error) {
      // catch the error
      mergeBackendValidation(error.response.status, error.response.data, methods.setError);
    }
  }


  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {/* render page title */}
      <Helmet><title>[{import.meta.env.VITE_APP_NAME}] Reset Password</title></Helmet>

      <Heading level="1">password reset</Heading>
      <FormProvider {...methods} >
        <form onSubmit={methods.handleSubmit(handleSendForm)}>
          <Input name='token' type='text' title='confirm token' className='h-16' />
          <Input name='password' type='password' title='new password' className='h-16' autoFocus={true} />
          <Input name='confirmPassword' type='password' title='confirm password' className='h-16' />

          <Submit value='Reset' />
        </form>
      </FormProvider>

      <div className="mt-4">
        <Link to="/login">Back to Login</Link>
      </div>
    </>
  );
}

export default React.memo(ResetPasswordForm);