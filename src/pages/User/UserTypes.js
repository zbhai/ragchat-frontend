// the _id represents the order ranking the role
// the lower the _id, the lower the permissions
export const ROLES = [
  { _id: 0, title: 'User' },
  { _id: 1, title: 'Editor' },
  { _id: 2, title: 'Poweruser' },
  { _id: 3, title: 'Lead' },
  { _id: 4, title: 'Admin' }
];