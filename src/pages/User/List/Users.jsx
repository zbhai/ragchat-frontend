import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import ConfirmBox from '../../../components/boxes/ConfirmBox';
import Heading from '../../../components/font/Heading';
import api from '../../../utils/AxiosConfig';
import { setFlashMsg } from '../../../utils/ErrorHandling';
import User from './User';
import { mergeBackendValidation } from '/src/utils/ErrorHandling';


function Users() {
  // #################################
  // HOOKS
  // #################################
  // ### USERS
  const [users, setUsers] = useState();

  // ### CONFIRM DIALOG
  const [confirmDialog, setConfirmDialog] = useState({ open: false, item: {} });

  // ### FETCH USERS
  useEffect(() => {
    // mount
    const controller = new AbortController();
    const getUsers = async () => {
      try {
        const result = await api.get('/users', {
          signal: controller.signal
        });
        setUsers(result?.data);
      } catch (error) {
        mergeBackendValidation(error.response.status, error.response.data);
      }
    };
    getUsers();

    return () => {
      // on unmount abort request
      controller.abort();
    };
  }, []);

  // #################################
  // FUNCTIONS
  // #################################
  // ### DELETE USERS
  const handleDelete = async (id) => {
    try {
      // delete in backend
      const result = await api.delete(`/users/${id}`);
      // delete in frontend
      const list = users.filter(user => user.id !== id);
      setUsers(list);
      setFlashMsg(result.data?.message);
    } catch (error) {

      console.log("🚀 ~ handleDelete ~ error:", error);


      mergeBackendValidation(error.response.status, error.response.data, methods.setError);
    }
  };

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {/* render page title */}
      <Helmet><title>[{import.meta.env.VITE_APP_NAME}] Users</title></Helmet>

      <Heading level="1" className="col-span-2">Registered Users</Heading>
      <div>
        {users?.length
          ? (
            <div className='flex flex-wrap justify-items-stretch'>
              {users.map((user, idx) =>
                <User key={user._id} idx={idx} user={user} setConfirmDialog={setConfirmDialog} />
              )}

            </div>
          ) : <p>No users to display</p>
        }
      </div>
      {/* confirmDialog */}
      <ConfirmBox confirmDialog={confirmDialog} closeDialog={() => setConfirmDialog({ ...confirmDialog, open: false })} handleProceed={() => { handleDelete(confirmDialog.idToDelete); }} />
    </>
  );
}

export default React.memo(Users);
