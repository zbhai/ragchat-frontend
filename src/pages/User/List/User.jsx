import { zodResolver } from '@hookform/resolvers/zod';
import React, { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { RiDeleteBinLine } from 'react-icons/ri';
import { z } from 'zod';
import Input from '../../../components/form/Input';
import Select from '../../../components/form/Select';
import Submit from '../../../components/form/Submit';
import { useAuth } from '../../../contexts/Auth/AuthState';
import { ROLES } from '../UserTypes';
import { mergeBackendValidation, setFlashMsg } from '/src/utils/ErrorHandling';


function User({ user, idx, setConfirmDialog }) {
  // #################################
  // VALIDATION SCHEMA
  // #################################
  const schema = z.object({
    name: z.string().min(2),
    username: z.string().min(2),
    email: z.string().min(1).email(),
    // turn string into boolean
    verified: z.string().toLowerCase().transform((x) => x === 'true').pipe(z.boolean()),
    role: z.coerce.number()
  });
  // #################################
  // HOOKS
  // #################################
  // ### CONNECT AUTH CONTEXT
  const { currentUser, update } = useAuth();
  const [updatedUser, setUpdatedUser] = useState(user);

  // ### PREPARE FORM
  const methods = useForm({
    resolver: zodResolver(schema),
    mode: 'onChange',
    // use anything from user, but turn verified into string
    defaultValues: { ...user, verified: user.verified.toString() }
  });


  // const id = useId();

  // #################################
  // FUNCTIONS
  // #################################
  // ### HANDLE SUBMITTING FORM
  async function handleSendForm(inputs) {
    // save to db
    try {
      // send data
      const result = await update(user._id, inputs);
      setUpdatedUser(result.data.document);
      // set flash msg
      setFlashMsg(result.data?.message);
    } catch (error) {
      // catch the error
      mergeBackendValidation(error.response.status, error.response.data, methods.setError);
    }
  }


  // #################################
  // OUTPUT
  // #################################
  return (
    <div className='flex items-center bg-white border border-UhhLightGrey rounded-lg shadow-lg p-3 m-3'>
      <div>
        {currentUser?.role >= 2 ?
          <>
            <h2 className='text-Uhh-Grey font-bold text-2xl flex justify-end'>
              <RiDeleteBinLine className='cursor-pointer hover:text-UhhRed' title='delete user' onClick={() => setConfirmDialog({ open: true, idToDelete: user.id, displayName: user.username })} />
            </h2>
            <FormProvider {...methods} >
              <form onSubmit={methods.handleSubmit(handleSendForm)}>
                <Input name='name' type='text' title='name' required={true} />

                <Input name='username' type='text' title='username' required={true} />

                <Input name='email' type='email' title='eMail' required={true} />

                <Select title="verified" name="verified" options={[{ _id: true, title: 'verified' }, { _id: false, title: 'not verified' }]} required={true} />

                <Select title="Role" name="role" options={ROLES} required={true} />

                <Submit value='save' />
              </form>
            </FormProvider>
          </>
          :
          <>
            <h2 className='text-Uhh-Grey font-bold text-2xl'>{user.fullname}</h2>
            <p className='text-sm'>{user.email}</p>
          </>
        }
      </div>
    </div>
  );
}

export default React.memo(User);