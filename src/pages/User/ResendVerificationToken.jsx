import { zodResolver } from '@hookform/resolvers/zod';
import React from 'react';
import { Helmet } from 'react-helmet-async';
import { FormProvider, useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { z } from 'zod';
import Heading from '../../components/font/Heading';
import Input from '../../components/form/Input';
import Submit from '../../components/form/Submit';
import { useAuth } from '../../contexts/Auth/AuthState';
import { mergeBackendValidation, setFlashMsg } from '../../utils/ErrorHandling';

function ForgotPassword() {
  // #################################
  // VALIDATION SCHEMA
  // #################################
  const schema = z.object({
    email: z.string().email(),
  });

  // #################################
  // HOOKS
  // #################################
  const methods = useForm({
    resolver: zodResolver(schema),
    mode: 'onSubmit'
  });

  // #################################
  // FUNCTIONS
  // #################################
  // ### IMPORT FUNCTION FROM AuthContext
  const { requestVerificationToken } = useAuth();

  // ### HANDLE SUBMITTING FORM
  async function handleSendForm(inputs) {
    try {
      // send data to function
      const result = await requestVerificationToken(inputs.email);
      // set flash message
      setFlashMsg(result.data?.message);
    } catch (error) {
      // catch the error
      mergeBackendValidation(error.response.status, error.response.data, methods.setError);
    }
  }

  // #################################
  // OUTPUT
  // #################################
  return (
    <>
      {/* render page title */}
      <Helmet><title>[{import.meta.env.VITE_APP_NAME}] Password forgot</title></Helmet>

      <Heading level="1">resend verification token</Heading>
      <FormProvider {...methods} >
        <form onSubmit={methods.handleSubmit(handleSendForm)}>
          <Input name='email' type='mail' title='E-Mail' className='h-16' required={true} />
          <Submit value='request' />
        </form>
      </FormProvider>

      <div className="my-4 flex justify-between">
        <Link to="/signup/insertToken">Back to Verification</Link>
      </div>
    </>
  );
}

export default React.memo(ForgotPassword);