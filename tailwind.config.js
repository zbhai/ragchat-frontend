/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: ["class"],
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}"
  ],
  theme: {
    container: {
      center: true,
      padding: "2rem",
      screens: {
        "2xl": "1400px",
      },
    },
    extend: {
      borderRadius: {
        lg: "var(--radius)",
        md: "calc(var(--radius) - 2px)",
        sm: "calc(var(--radius) - 4px)",
      },
      keyframes: {
        "accordion-down": {
          from: { height: 0 },
          to: { height: "var(--radix-accordion-content-height)" },
        },
        "accordion-up": {
          from: { height: "var(--radix-accordion-content-height)" },
          to: { height: 0 },
        },
      },
      animation: {
        "accordion-down": "accordion-down 0.2s ease-out",
        "accordion-up": "accordion-up 0.2s ease-out",
      },
      colors: {
        UhhBlue: '#0271bb',
        UhhLightBlue: 'rgba(128, 184, 219, 0.5)',
        UhhRed: '#e2001a',
        UhhLightRed: 'rgba(240, 130, 136, 0.5)',
        UhhGrey: '#3b515b',
        UhhLightGrey: '#f3f3f3',
        UhhWhite: '#fff',
        UhhWLightWhite: '#FFFAF0',
        border: "hsl(var(--border))",
        input: "hsl(var(--input))",
        ring: "hsl(var(--ring))",
        background: "hsl(var(--background))",
        foreground: "hsl(var(--foreground))",
        primary: {
          DEFAULT: "hsl(var(--primary))",
          foreground: "hsl(var(--primary-foreground))",
        },
        secondary: {
          DEFAULT: "hsl(var(--secondary))",
          foreground: "hsl(var(--secondary-foreground))",
        },
        destructive: {
          DEFAULT: "hsl(var(--destructive))",
          foreground: "hsl(var(--destructive-foreground))",
        },
        muted: {
          DEFAULT: "hsl(var(--muted))",
          foreground: "hsl(var(--muted-foreground))",
        },
        accent: {
          DEFAULT: "hsl(var(--accent))",
          foreground: "hsl(var(--accent-foreground))",
        },
        popover: {
          DEFAULT: "hsl(var(--popover))",
          foreground: "hsl(var(--popover-foreground))",
        },
        card: {
          DEFAULT: "hsl(var(--card))",
          foreground: "hsl(var(--card-foreground))",
        },
      },
      spacing: {
        '1/7': '14.285%'
      },
      fontFamily: {
        'UhhSLC': ['TheSansUHHSemiLightCaps'],
        'UhhB': ['TheSansUHHBold'],
        'UhhBC': ['TheSansUHHBoldCaps'],
        'UhhR': ['TheSansUHHRegular'],
        'Uhh': ['TheSansUHH'],
        'UhhRC': ['TheSansUHHRegularCaps'],
        'UhhI': ['TheSansUHHItalic'],
      },
      transitionProperty: {
        'width': 'width'
      },
      borderColor: {
        DEFAULT: '#3b515b'
      },
      minWidth: {
        'xs': '20rem',
        'sm': '24rem',
        'md': '28rem',
        'lg': '32rem',
        'xl': '36rem',
        '2xl': '42rem',
      }
    }
  },
  plugins: [],
};
